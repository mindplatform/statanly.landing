'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _App = require('UI/App/App.react');

var _App2 = _interopRequireDefault(_App);

var _reactRouter = require('react-router');

var _reactRedux = require('react-redux');

var _server = require('react-dom/server');

var _server2 = _interopRequireDefault(_server);

var _reactI18next = require('react-i18next');

var _i18n = require('Localization/i18n');

var _i18n2 = _interopRequireDefault(_i18n);

var _Store = require('Store/Store');

var _Store2 = _interopRequireDefault(_Store);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _server2.default.renderToStaticMarkup(_react2.default.createElement(
    _reactRouter.StaticRouter,
    { location: '/', context: {} },
    _react2.default.createElement(
        _reactI18next.I18nextProvider,
        { i18n: _i18n2.default },
        _react2.default.createElement(
            _reactRedux.Provider,
            { store: _Store2.default },
            _react2.default.createElement(_App2.default, null)
        )
    )
));