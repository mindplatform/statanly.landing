module.exports = {
  "rewrite": [
    { "from": "/Account/LoginAsync", "to": "http://statanly.com/Account/LoginAsync" },
    { "from": "/Account/RegisterAsync", "to": "http://statanly.com/Account/RegisterAsync" },
    { "from": "/Message/SendAsync", "to": "http://statanly.com/Message/SendAsync" },
    { "from": "/Message/SendRequestAsync", "to": "http://statanly.com/Message/SendRequestAsync" }
  ]
}
