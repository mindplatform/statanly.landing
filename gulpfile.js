var gulp = require('gulp');
var less = require('gulp-less');
var concat = require('gulp-concat');
var path = require('path');
var run = require('gulp-run');
var babel = require('gulp-babel');

gulp.task('less', function () {
  return gulp.src('./src/**/*.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(concat('home.css'))
    .pipe(gulp.dest('./public/content/spa/css'));
});


gulp.task('react', function () {
    return gulp.src('./src/scripts/Home.js')
    .pipe(babel({
        presets: ['env', 'es2015', 'react', 'stage-0'],
        plugins: ['transform-decorators-legacy']
    }))
    .pipe(gulp.dest('./static/templates'))
})
