import React from 'react'
import App from 'UI/App/App.react'

import {
    StaticRouter,
} from 'react-router'
import { Provider } from 'react-redux'
import ReactDOMServer from 'react-dom/server'
import {
    I18nextProvider,
} from 'react-i18next'
import i18n from 'Localization/i18n'

import store from 'Store/Store'

export default ReactDOMServer.renderToStaticMarkup((
    <StaticRouter location="/" context={{}}>
        <I18nextProvider i18n={i18n}>
            <Provider store={store}>
                <App/>
            </Provider>
        </I18nextProvider>
    </StaticRouter>
))
