import createBrowserHistory from 'history/createBrowserHistory'
import { createStore, combineReducers, applyMiddleware } from 'redux'
import { routerReducer, routerMiddleware } from 'react-router-redux'

import thunk from 'redux-thunk'
import API_HTTP_M from 'Redux/Middleware/API.HTTP.middleware'
import Auth_M from 'Redux/Middleware/Auth/Auth.middleware'
import Interactive_M from 'Redux/Middleware/Interactive/Interactive.middleware'

import * as Reducers from 'Redux/Reducers'

const _browserHistory = createBrowserHistory()

const middleware = [
    routerMiddleware(_browserHistory),
    thunk,
    API_HTTP_M,
    Auth_M,
    Interactive_M,
]

const store = createStore(
    combineReducers({
        ...Reducers,
        routing: routerReducer
    }),
    applyMiddleware(...middleware)
)

export default store