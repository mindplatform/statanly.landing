import I from 'immutable'
import {
    createSelector,
} from 'reselect'

const isAuthenticated = ({ state }) =>
    state.UserAccount.getIn(['state', 'isAuthenticated'])

const getEmail = ({ state }) =>
    state.UserAccount.getIn(['state', 'email'])

export default createSelector(
    [
        isAuthenticated,
        getEmail,
    ],
    (
        isAuthenticated,
        email,
    ) => ({
        isAuthenticated,
        email,
    })
)
