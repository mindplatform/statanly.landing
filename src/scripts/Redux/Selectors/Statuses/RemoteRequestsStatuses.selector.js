import I from 'immutable'
import {
    createSelector,
} from 'reselect'

const isAnyRemoteRequestInProcess = ({ state }) =>
    state.AuthRegister.getIn(['state', 'isRequestProcess']) ||
    state.AuthLogin.getIn(['state', 'isRequestProcess']) ||
    state.InteractiveFeedback.getIn(['state', 'isRequestProcess']) ||
    state.InteractiveRequest.getIn(['state', 'isRequestProcess'])

export default createSelector(
    [
        isAnyRemoteRequestInProcess,
    ],
    (
        isAnyRemoteRequestInProcess,
    ) => ({
        isAnyRemoteRequestInProcess,
    })
)
