import I from 'immutable'
import {
    createSelector,
} from 'reselect'

const isFeedbackInProcess = ({ state }) =>
    state.InteractiveFeedback.getIn(['state', 'isRequestProcess'])

export default createSelector(
    [
        isFeedbackInProcess,
    ],
    (
        isFeedbackInProcess,
    ) => ({
        isFeedbackInProcess,
    })
)
