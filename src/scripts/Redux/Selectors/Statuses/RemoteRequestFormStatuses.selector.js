import I from 'immutable'
import {
    createSelector,
} from 'reselect'

const isRequestInProcess = ({ state }) =>
    state.InteractiveRequest.getIn(['state', 'isRequestProcess'])

export default createSelector(
    [
        isRequestInProcess,
    ],
    (
        isRequestInProcess,
    ) => ({
        isRequestInProcess,
    })
)
