import {
    REMOTE_INTERACTIVE_FEEDBACK_REQUEST,
    REMOTE_INTERACTIVE_FEEDBACK_SUCCESS,
    REMOTE_INTERACTIVE_FEEDBACK_FAILURE,
} from 'Redux/Actions/Interactive/Feedback.actions'

import {
    REMOTE_INTERACTIVE_REQUEST_REQUEST,
    REMOTE_INTERACTIVE_REQUEST_SUCCESS,
    REMOTE_INTERACTIVE_REQUEST_FAILURE,
} from 'Redux/Actions/Interactive/Request.actions'

import EventMaster from 'Helpers/EventMaster/EventMaster.helper'

export default store => next => action => {

    next(action)

    switch (action.type) {

        case REMOTE_INTERACTIVE_FEEDBACK_SUCCESS:
            (({
                isSuccess,
                message,
            }) => {
                if (isSuccess) {
                    EventMaster.emit(action.type)
                }
            })(action.response)
            break

        case REMOTE_INTERACTIVE_REQUEST_SUCCESS:
            (({
                isSuccess,
                message,
            }) => {
                if (isSuccess) {
                    EventMaster.emit(action.type)
                }
            })(action.response)
            break

    }
}
