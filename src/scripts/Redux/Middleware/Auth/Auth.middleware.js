import {
    REMOTE_AUTH_LOGIN_REQUEST,
    REMOTE_AUTH_LOGIN_SUCCESS,
    REMOTE_AUTH_LOGIN_FAILURE,
} from 'Redux/Actions/Auth/Login.actions'

import {
    REMOTE_AUTH_REGISTER_REQUEST,
    REMOTE_AUTH_REGISTER_SUCCESS,
    REMOTE_AUTH_REGISTER_FAILURE,
} from 'Redux/Actions/Auth/Register.actions'

import {
    addUserAccountInfo,
} from 'Redux/Actions/UserAccount/UserAccount.actions'

import EventMaster from 'Helpers/EventMaster/EventMaster.helper'

export default store => next => action => {

    next(action)

    switch (action.type) {

        case REMOTE_AUTH_LOGIN_SUCCESS:
        case REMOTE_AUTH_REGISTER_SUCCESS:
            (({
                isAuthenticated,
                email,
            }) => {
                store.dispatch(addUserAccountInfo({
                    isAuthenticated,
                    email,
                }))
            })(action.response.data)
            break

        case REMOTE_AUTH_LOGIN_FAILURE:
        case REMOTE_AUTH_REGISTER_FAILURE:
            (({
                isSuccess,
            }) => {
                if (!isSuccess) {
                    EventMaster.emit(action.type)
                }
            })(action.error)
            break

    }
}
