import I from 'immutable'
import CONFIG from 'Configuration/Config'
import {
    reduce,
} from 'lodash'

const LOCAL_ORIGIN = document.location.protocol + '//' + document.location.host
const API_ROOT = CONFIG.get('API_ROOT_HTTP') || `${LOCAL_ORIGIN}/`

function FETCH_API (route, data, body2Send) {
    const REQUEST_PARAMS = route.get('params').toJS()
    const METHOD = route.getIn(['params', 'method'], 'GET')
    const ROUTE = ~route.get('path').indexOf('http://') || ~route.get('path').indexOf('https://')
        ? route.get('path')
        : (
            route.get('isLocal')
                ? LOCAL_ORIGIN
                : API_ROOT
        ).concat(route.get('path'))

    const URL = new window.URL(ROUTE)
    const search = I.fromJS(route.get('search'))
    if (search && search.size) {
        const SEARCH_PARAMS = reduce(search.toJS(), (result, value, key) => result.concat(key, '=', value, '&'), '').slice(0,-1)
        URL.search = SEARCH_PARAMS
    }

    const body = {
        body: body2Send || JSON.stringify(data),
    }

    return fetch(
        URL.href,
        Object.assign(
            REQUEST_PARAMS,
            ~METHOD.toLowerCase().indexOf('get') || ~METHOD.toLowerCase().indexOf('head') ? {} : body,
        )

    ).then(response => {
        const statusCodeFirstChar = response.status.toString().charAt(0)
        if (statusCodeFirstChar == 4 || statusCodeFirstChar == 5)
            return Promise.reject(response)

        const contentType = response.headers.get("content-type")
        if (contentType && ~contentType.indexOf("application/json"))
            return response.json().then(json => ({ json, response }))

        return Promise.reject(response)

    }).then(({ json, response }) => {

        if (!response.ok) {
            return Promise.reject(json)
        }

        if (!json.isSuccess) {
            return Promise.reject(json)
        }

        return json
    })
}

export const API_CALL = Symbol('API_Symbol')

export default store => next => action => {
    const API_CALL_DATA = action[API_CALL]

    if (typeof API_CALL_DATA === 'undefined') {
        return next(action);
    }

    const {
        route,
        types,
        data,
        body,
        echo,
    } = API_CALL_DATA;

    if (!route) {
        throw new Error('Route parameter is undefined.')
    }
    if (!Array.isArray(types) || types.length !== 3) {
        throw new Error('Expected an array of three action types.')
    }
    if (!types.every(type => typeof type === 'string')) {
        throw new Error('Expected action types to be strings.')
    }

    function constructRequestParams (extra) {
        const fullParams = Object.assign({}, action, extra)
        delete fullParams[API_CALL]
        return fullParams
    }

    const [ requestType, successType, failureType ] = types
    next(constructRequestParams({ type: requestType }))

    return FETCH_API(route, data, body).then(
        response => next(constructRequestParams({
            type: successType,
            response,
            echo,
        })),
        error => next(constructRequestParams({
            type: failureType,
            error,
            echo,
        }))
    )
}
