import {
    API_CALL,
} from 'Redux/Middleware/API.HTTP.middleware'
import ROUTES from 'Routes/Routes.HTTP'

export const REMOTE_AUTH_LOGIN_REQUEST = "REMOTE_AUTH_LOGIN_REQUEST"
export const REMOTE_AUTH_LOGIN_SUCCESS = "REMOTE_AUTH_LOGIN_SUCCESS"
export const REMOTE_AUTH_LOGIN_FAILURE = "REMOTE_AUTH_LOGIN_FAILURE"

export function authLogin ({
    body,
    data,
    search,
    echo,
} = {}) {
    return {
        [API_CALL]: {
            route: ROUTES
                        .getIn(['auth', 'login'])
                        .setIn(['search'], search),
            types: [
                REMOTE_AUTH_LOGIN_REQUEST,
                REMOTE_AUTH_LOGIN_SUCCESS,
                REMOTE_AUTH_LOGIN_FAILURE,
            ],
            data,
            body,
            echo,
        }
    }
}
