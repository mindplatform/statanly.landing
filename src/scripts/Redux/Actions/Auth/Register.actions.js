import {
    API_CALL,
} from 'Redux/Middleware/API.HTTP.middleware'
import ROUTES from 'Routes/Routes.HTTP'

export const REMOTE_AUTH_REGISTER_REQUEST = "REMOTE_AUTH_REGISTER_REQUEST"
export const REMOTE_AUTH_REGISTER_SUCCESS = "REMOTE_AUTH_REGISTER_SUCCESS"
export const REMOTE_AUTH_REGISTER_FAILURE = "REMOTE_AUTH_REGISTER_FAILURE"

export function authRegister ({
    body,
    data,
    search,
    echo,
} = {}) {
    return {
        [API_CALL]: {
            route: ROUTES
                        .getIn(['auth', 'register'])
                        .setIn(['search'], search),
            types: [
                REMOTE_AUTH_REGISTER_REQUEST,
                REMOTE_AUTH_REGISTER_SUCCESS,
                REMOTE_AUTH_REGISTER_FAILURE,
            ],
            data,
            body,
            echo,
        }
    }
}
