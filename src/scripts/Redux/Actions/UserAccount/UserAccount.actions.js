export const ADD_USER_ACCOUNT_INFO = "ADD_USER_ACCOUNT_INFO"
export function addUserAccountInfo ({
    isAuthenticated,
    email,
} = {}) {
    return {
        type: ADD_USER_ACCOUNT_INFO,
        isAuthenticated,
        email,
    }
}
