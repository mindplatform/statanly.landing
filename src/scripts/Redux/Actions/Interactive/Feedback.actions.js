import {
    API_CALL,
} from 'Redux/Middleware/API.HTTP.middleware'
import ROUTES from 'Routes/Routes.HTTP'

export const REMOTE_INTERACTIVE_FEEDBACK_REQUEST = "REMOTE_INTERACTIVE_FEEDBACK_REQUEST"
export const REMOTE_INTERACTIVE_FEEDBACK_SUCCESS = "REMOTE_INTERACTIVE_FEEDBACK_SUCCESS"
export const REMOTE_INTERACTIVE_FEEDBACK_FAILURE = "REMOTE_INTERACTIVE_FEEDBACK_FAILURE"

export function interactiveFeedback ({
    body,
    data,
    search,
    echo,
} = {}) {
    return {
        [API_CALL]: {
            route: ROUTES
                        .getIn(['interactive', 'feedback'])
                        .setIn(['search'], search),
            types: [
                REMOTE_INTERACTIVE_FEEDBACK_REQUEST,
                REMOTE_INTERACTIVE_FEEDBACK_SUCCESS,
                REMOTE_INTERACTIVE_FEEDBACK_FAILURE,
            ],
            data,
            body,
            echo,
        }
    }
}
