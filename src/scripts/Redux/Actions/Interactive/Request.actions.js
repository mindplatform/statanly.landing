import {
    API_CALL,
} from 'Redux/Middleware/API.HTTP.middleware'
import ROUTES from 'Routes/Routes.HTTP'

export const REMOTE_INTERACTIVE_REQUEST_REQUEST = "REMOTE_INTERACTIVE_REQUEST_REQUEST"
export const REMOTE_INTERACTIVE_REQUEST_SUCCESS = "REMOTE_INTERACTIVE_REQUEST_SUCCESS"
export const REMOTE_INTERACTIVE_REQUEST_FAILURE = "REMOTE_INTERACTIVE_REQUEST_FAILURE"

export function interactiveRequest ({
    body,
    data,
    search,
    echo,
} = {}) {
    return {
        [API_CALL]: {
            route: ROUTES
                        .getIn(['interactive', 'request'])
                        .setIn(['search'], search),
            types: [
                REMOTE_INTERACTIVE_REQUEST_REQUEST,
                REMOTE_INTERACTIVE_REQUEST_SUCCESS,
                REMOTE_INTERACTIVE_REQUEST_FAILURE,
            ],
            data,
            body,
            echo,
        }
    }
}
