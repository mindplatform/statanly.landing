import I from 'immutable'

import {
    ADD_USER_ACCOUNT_INFO,
} from 'Redux/Actions/UserAccount/UserAccount.actions'

const DEFAULT_STATE = I.fromJS({
    state: {},
})

export default function UserAccountReducer (state = DEFAULT_STATE, action) {
    switch (action.type) {

        case ADD_USER_ACCOUNT_INFO:
            return (nextState => nextState
                .setIn(['state', 'isAuthenticated'], action.isAuthenticated)
                .setIn(['state', 'email'], action.email)
            )(state)

    }
    return state
}
