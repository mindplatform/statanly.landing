import I from 'immutable'

import {
    REMOTE_INTERACTIVE_REQUEST_REQUEST,
    REMOTE_INTERACTIVE_REQUEST_SUCCESS,
    REMOTE_INTERACTIVE_REQUEST_FAILURE,
} from 'Redux/Actions/Interactive/Request.actions'

const DEFAULT_STATE = I.fromJS({
    state: {},
})

export default function RequestReducer (state = DEFAULT_STATE, action) {
    switch (action.type) {
        case REMOTE_INTERACTIVE_REQUEST_REQUEST:
            return (nextState => nextState
                .setIn(['state', 'isRequestProcess'], true)
                .setIn(['state', 'isRequestSuccess'], false)
                .setIn(['state', 'isRequestFailure'], false)
            )(state)

        case REMOTE_INTERACTIVE_REQUEST_SUCCESS:
            return (nextState => nextState
                .setIn(['state', 'isRequestProcess'], false)
                .setIn(['state', 'isRequestSuccess'], true)
                .setIn(['state', 'isRequestFailure'], false)
            )(state)

        case REMOTE_INTERACTIVE_REQUEST_FAILURE:
            return (nextState => nextState
                .setIn(['state', 'isRequestProcess'], false)
                .setIn(['state', 'isRequestSuccess'], false)
                .setIn(['state', 'isRequestFailure'], true)
            )(state)
    }
    return state
}
