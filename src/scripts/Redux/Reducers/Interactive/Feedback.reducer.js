import I from 'immutable'

import {
    REMOTE_INTERACTIVE_FEEDBACK_REQUEST,
    REMOTE_INTERACTIVE_FEEDBACK_SUCCESS,
    REMOTE_INTERACTIVE_FEEDBACK_FAILURE,
} from 'Redux/Actions/Interactive/Feedback.actions'

const DEFAULT_STATE = I.fromJS({
    state: {},
})

export default function FeedbackReducer (state = DEFAULT_STATE, action) {
    switch (action.type) {
        case REMOTE_INTERACTIVE_FEEDBACK_REQUEST:
            return (nextState => nextState
                .setIn(['state', 'isRequestProcess'], true)
                .setIn(['state', 'isRequestSuccess'], false)
                .setIn(['state', 'isRequestFailure'], false)
            )(state)

        case REMOTE_INTERACTIVE_FEEDBACK_SUCCESS:
            return (nextState => nextState
                .setIn(['state', 'isRequestProcess'], false)
                .setIn(['state', 'isRequestSuccess'], true)
                .setIn(['state', 'isRequestFailure'], false)
            )(state)

        case REMOTE_INTERACTIVE_FEEDBACK_FAILURE:
            return (nextState => nextState
                .setIn(['state', 'isRequestProcess'], false)
                .setIn(['state', 'isRequestSuccess'], false)
                .setIn(['state', 'isRequestFailure'], true)
            )(state)
    }
    return state
}
