import I from 'immutable'

import {
    REMOTE_AUTH_REGISTER_REQUEST,
    REMOTE_AUTH_REGISTER_SUCCESS,
    REMOTE_AUTH_REGISTER_FAILURE,
} from 'Redux/Actions/Auth/Register.actions'

const DEFAULT_STATE = I.fromJS({
    state: {},
})

export default function RegisterReducer (state = DEFAULT_STATE, action) {
    switch (action.type) {
        case REMOTE_AUTH_REGISTER_REQUEST:
            return (nextState => nextState
                .setIn(['state', 'isRequestProcess'], true)
                .setIn(['state', 'isRequestSuccess'], false)
                .setIn(['state', 'isRequestFailure'], false)
            )(state)

        case REMOTE_AUTH_REGISTER_SUCCESS:
            return (nextState => nextState
                .setIn(['state', 'isRequestProcess'], false)
                .setIn(['state', 'isRequestSuccess'], true)
                .setIn(['state', 'isRequestFailure'], false)
            )(state)

        case REMOTE_AUTH_REGISTER_FAILURE:
            return (nextState => nextState
                .setIn(['state', 'isRequestProcess'], false)
                .setIn(['state', 'isRequestSuccess'], false)
                .setIn(['state', 'isRequestFailure'], true)
            )(state)
    }
    return state
}
