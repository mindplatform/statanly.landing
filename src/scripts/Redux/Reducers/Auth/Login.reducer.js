import I from 'immutable'

import {
    REMOTE_AUTH_LOGIN_REQUEST,
    REMOTE_AUTH_LOGIN_SUCCESS,
    REMOTE_AUTH_LOGIN_FAILURE,
} from 'Redux/Actions/Auth/Login.actions'

const DEFAULT_STATE = I.fromJS({
    state: {},
})

export default function LoginReducer (state = DEFAULT_STATE, action) {
    switch (action.type) {
        case REMOTE_AUTH_LOGIN_REQUEST:
            return (nextState => nextState
                .setIn(['state', 'isRequestProcess'], true)
                .setIn(['state', 'isRequestSuccess'], false)
                .setIn(['state', 'isRequestFailure'], false)
            )(state)

        case REMOTE_AUTH_LOGIN_SUCCESS:
            return (nextState => nextState
                .setIn(['state', 'isRequestProcess'], false)
                .setIn(['state', 'isRequestSuccess'], true)
                .setIn(['state', 'isRequestFailure'], false)
            )(state)

        case REMOTE_AUTH_LOGIN_FAILURE:
            return (nextState => nextState
                .setIn(['state', 'isRequestProcess'], false)
                .setIn(['state', 'isRequestSuccess'], false)
                .setIn(['state', 'isRequestFailure'], true)
            )(state)
    }
    return state
}
