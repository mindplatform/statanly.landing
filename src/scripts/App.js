import React from 'react'
import { render } from 'react-dom'
import RouterMap from 'UI/Router/Map.router'

render(RouterMap, document.getElementById('Stage'))


// import App from 'UI/App/App.react'

// import {
//     StaticRouter,
// } from 'react-router'
// import { Provider } from 'react-redux'
// import ReactDOMServer from 'react-dom/server'
// import {
//     I18nextProvider,
// } from 'react-i18next'
// import i18n from 'Localization/i18n'

// import store from 'Store/Store'

// console.log('\n\n Home:')
// console.log(ReactDOMServer.renderToStaticMarkup((
//     <StaticRouter location="/" context={{}}>
//         <I18nextProvider i18n={i18n}>
//             <Provider store={store}>
//                 <App/>
//             </Provider>
//         </I18nextProvider>
//     </StaticRouter>
// )))
// console.log('\n\n About:')
// console.log(ReactDOMServer.renderToStaticMarkup((
//     <StaticRouter location="/about/company" context={{}}>
//         <I18nextProvider i18n={i18n}>
//             <Provider store={store}>
//                 <App/>
//             </Provider>
//         </I18nextProvider>
//     </StaticRouter>
// )))
// console.log('\n\n Help:')
// console.log(ReactDOMServer.renderToStaticMarkup((
//     <StaticRouter location="/help" context={{}}>
//         <I18nextProvider i18n={i18n}>
//             <Provider store={store}>
//                 <App/>
//             </Provider>
//         </I18nextProvider>
//     </StaticRouter>
// )))
// console.log('\n\n Login:')
// console.log(ReactDOMServer.renderToStaticMarkup((
//     <StaticRouter location="/Account/Login" context={{}}>
//         <I18nextProvider i18n={i18n}>
//             <Provider store={store}>
//                 <App/>
//             </Provider>
//         </I18nextProvider>
//     </StaticRouter>
// )))
// console.log('\n\n Register:')
// console.log(ReactDOMServer.renderToStaticMarkup((
//     <StaticRouter location="/Account/Register" context={{}}>
//         <I18nextProvider i18n={i18n}>
//             <Provider store={store}>
//                 <App/>
//             </Provider>
//         </I18nextProvider>
//     </StaticRouter>
// )))
// console.log('\n\n Services:')
// console.log(ReactDOMServer.renderToStaticMarkup((
//     <StaticRouter location="/services/core" context={{}}>
//         <I18nextProvider i18n={i18n}>
//             <Provider store={store}>
//                 <App/>
//             </Provider>
//         </I18nextProvider>
//     </StaticRouter>
// )))
// console.log('\n\n Research:')
// console.log(ReactDOMServer.renderToStaticMarkup((
//     <StaticRouter location="/research" context={{}}>
//         <I18nextProvider i18n={i18n}>
//             <Provider store={store}>
//                 <App/>
//             </Provider>
//         </I18nextProvider>
//     </StaticRouter>
// )))
// console.log('\n\n')
