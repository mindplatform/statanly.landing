import './RequestForm.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import RequestFormComponent from 'UI/Components/Interactive/RequestForm.react'

import * as InteractiveRequestActions from 'Redux/Actions/Interactive/Request.actions'
import RemoteRequestFormStatusesSelector from 'Redux/Selectors/Statuses/RemoteRequestFormStatuses.selector'

import {
    REMOTE_INTERACTIVE_REQUEST_SUCCESS,
    REMOTE_INTERACTIVE_REQUEST_FAILURE,
} from 'Redux/Actions/Interactive/Request.actions'
import EventMaster from 'Helpers/EventMaster/EventMaster.helper'

class RequestForm extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'RequestForm'
        this._b = _b(this.boxClassName)

        this.state = {
            requestSucceeded: false,
            requestFailure: false,
        }

        this.handleRequestSuccess = ::this.handleRequestSuccess
        this.handleRequestFailure = ::this.handleRequestFailure
    }

    handleRequestSuccess () {
        this.setState({
            requestSucceeded: true,
        })
    }

    handleRequestFailure () {
        this.setState({
            requestFailure: true,
        })
        setTimeout(() => this.setState({
            requestFailure: false,
        }), 2000)
    }

    componentDidMount () {
        window.scrollTo(0,0)
        EventMaster.addListener(REMOTE_INTERACTIVE_REQUEST_SUCCESS, this.handleRequestSuccess)
        EventMaster.addListener(REMOTE_INTERACTIVE_REQUEST_FAILURE, this.handleRequestFailure)
    }

    componentWillUnmount () {
        EventMaster.removeListener(REMOTE_INTERACTIVE_REQUEST_SUCCESS, this.handleRequestSuccess)
        EventMaster.removeListener(REMOTE_INTERACTIVE_REQUEST_FAILURE, this.handleRequestFailure)
    }

    render () {
        return (
            <section className={this._b}>
                <RequestFormComponent
                    onSubmit={this.props.InteractiveRequestActions.interactiveRequest}
                    isInProcess={this.props.requestStates.isRequestInProcess}
                    isSucceeded={this.state.requestSucceeded}
                    isFailure={this.state.requestFailure}
                />
            </section>
        )
    }
}

export default connect(
    state => ({
        requestStates: RemoteRequestFormStatusesSelector({ state }),
    }),
    dispatch => ({
        InteractiveRequestActions: bindActionCreators(InteractiveRequestActions, dispatch),
    }),
)(RequestForm)
