import './FeedbackForm.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import FeedbackFormComponent from 'UI/Components/Interactive/FeedbackForm.react'

import * as InteractiveFeedbackActions from 'Redux/Actions/Interactive/Feedback.actions'
import RemoteFeedbackFormStatusesSelector from 'Redux/Selectors/Statuses/RemoteFeedbackFormStatuses.selector'

import {
    REMOTE_INTERACTIVE_FEEDBACK_SUCCESS,
    REMOTE_INTERACTIVE_FEEDBACK_FAILURE,
} from 'Redux/Actions/Interactive/Feedback.actions'
import EventMaster from 'Helpers/EventMaster/EventMaster.helper'

class FeedbackForm extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'FeedbackForm'
        this._b = _b(this.boxClassName)

        this.state = {
            feedbackSucceeded: false,
            feedbackFailure: false,
        }

        this.handleFeedBackSuccess = ::this.handleFeedBackSuccess
        this.handleFeedBackFailure = ::this.handleFeedBackFailure
    }

    handleFeedBackSuccess () {
        this.setState({
            feedbackSucceeded: true,
        })
    }

    handleFeedBackFailure () {
        this.setState({
            feedbackFailure: true,
        })
        setTimeout(() => this.setState({
            feedbackFailure: false,
        }), 2000)
    }

    componentDidMount () {
        window.scrollTo(0,0)
        EventMaster.addListener(REMOTE_INTERACTIVE_FEEDBACK_SUCCESS, this.handleFeedBackSuccess)
        EventMaster.addListener(REMOTE_INTERACTIVE_FEEDBACK_FAILURE, this.handleFeedBackFailure)
    }

    componentWillUnmount () {
        EventMaster.removeListener(REMOTE_INTERACTIVE_FEEDBACK_SUCCESS, this.handleFeedBackSuccess)
        EventMaster.removeListener(REMOTE_INTERACTIVE_FEEDBACK_FAILURE, this.handleFeedBackFailure)
    }

    render () {
        return (
            <section className={this._b}>
                <FeedbackFormComponent
                    onSubmit={this.props.InteractiveFeedbackActions.interactiveFeedback}
                    isInProcess={this.props.requestStates.isFeedbackInProcess}
                    isSucceeded={this.state.feedbackSucceeded}
                    isFailure={this.state.feedbackFailure}
                />
            </section>
        )
    }
}

export default connect(
    state => ({
        requestStates: RemoteFeedbackFormStatusesSelector({ state }),
    }),
    dispatch => ({
        InteractiveFeedbackActions: bindActionCreators(InteractiveFeedbackActions, dispatch),
    }),
)(FeedbackForm)
