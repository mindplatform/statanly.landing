import './AuthLoginButton.less'
import React, { Component } from 'react'
import bem from 'Helpers/Bem/Bem.helper'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import {
    translate,
} from 'react-i18next'

import UserAccountSelector from 'Redux/Selectors/UserAccount/UserAccount.selector'

class AuthLoginButton extends Component {
    constructor (props) {
        super(props)
        this.bem = bem.with("AuthLoginButton")
    }

    render () {
        const {
            t,
        } = this.props

        const {
            isAuthenticated,
        } = this.props.userAccount

        if (!isAuthenticated) {
            return (
                <a className={this.bem()} href="/Account/Login">{t('Auth.Sign In')}</a>
            )
        }

        return null
    }
}

export default connect(
    state => ({
        userAccount: UserAccountSelector({ state }),
    }),
    dispatch => ({}),
)(translate('translations')(AuthLoginButton))
