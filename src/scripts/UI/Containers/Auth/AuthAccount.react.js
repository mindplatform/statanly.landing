import './AuthAccount.less'
import React, { Component } from 'react'
import bem from 'Helpers/Bem/Bem.helper'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import {
    translate,
} from 'react-i18next'

import UserAccountSelector from 'Redux/Selectors/UserAccount/UserAccount.selector'
import UserAccount from 'UI/Containers/UserAccount/UserAccount.react'

import AuthLoginButton from './AuthLoginButton.react'

class AuthAccount extends Component {
    constructor (props) {
        super(props)
        this.bem = bem.with("AuthAccount")
    }

    render () {
        const {
            email,
            isAuthenticated,
        } = this.props.userAccount

        if (isAuthenticated) {
            return (
                <UserAccount/>
            )
        } else {
            return (
                <div className={this.bem({ green: !isAuthenticated })}>
                    <AuthLoginButton/>
                </div>
            )
        }
    }
}

export default connect(
    state => ({
        userAccount: UserAccountSelector({ state }),
    }),
    dispatch => ({}),
)(translate('translations')(AuthAccount))
