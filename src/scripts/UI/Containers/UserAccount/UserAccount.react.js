import './UserAccount.less'
import React, { Component } from 'react'
import _b from 'bem-cn'
import bem from 'Helpers/Bem/Bem.helper'
import classnames from 'classnames'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import UserAccountSelector from 'Redux/Selectors/UserAccount/UserAccount.selector'

import {
    translate,
} from 'react-i18next'

class ChevronRight extends Component {
    constructor (props) {
        super(props)
        this.blockName = "ChevronRight"
    }

    render () {
        return <div className={bem(this.blockName)}/>
    }
}

const UserAccountBody = translate('translations')(
    class __UserAccountBody extends Component {
        constructor (props) {
            super(props)
            this.blockName = "UserAccountBody"
        }

        render () {
            if (!this.props.visible)
                return null

            const {
                t,
            } = this.props

            return (
                <div className={classnames(bem(this.blockName), 'unselectable')}>
                    <div className={bem(this.blockName, 'Inner')}>
                        <a
                            className={bem(this.blockName, 'AccountLink')}
                            href="//app.statanly.com/"
                            target="_blank"
                        >
                            {t('Account.Account')}
                            <ChevronRight/>
                        </a>
                        <a
                            className={bem(this.blockName, 'LogoutLink')}
                            href="/account/logoff"
                        >
                            {t('Auth.Log Off')}
                            <ChevronRight/>
                        </a>
                    </div>
                </div>
            )
        }
    }
)

class UserAccount extends Component {
    constructor (props) {
        super(props)
        this.blockName = 'UserAccount'

        this.state = {
            active: false,
        }
    }

    render () {
        const {
            t,
        } = this.props

        const {
            email,
            isAuthenticated,
        } = this.props.userAccount

        if (!isAuthenticated)
            return null

        return (
            <div
                className={bem(this.blockName, null, {active: this.state.active})}
                onClick={() => {
                    this.setState(prevState => ({
                        active: !prevState.active,
                    }))
                }}
            >
                <div className={bem(this.blockName, 'UserName')}>
                    {email || 'Guest User'}
                </div>
                <div className={bem(this.blockName, 'UserPic')}/>
                <div className={bem(this.blockName, 'Chevron', {active: this.state.active})}/>
                <UserAccountBody visible={this.state.active}/>
            </div>
        )
    }
}

export default connect(
    state => ({
        userAccount: UserAccountSelector({ state }),
    }),
    dispatch => ({}),
)(translate('translations')(UserAccount))
