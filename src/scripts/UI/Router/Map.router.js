import React from 'react'
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom'
import { Provider } from 'react-redux'

import {
    I18nextProvider,
} from 'react-i18next'
import i18n from 'Localization/i18n'

import createBrowserHistory from 'history/createBrowserHistory'
import store from 'Store/Store'

import App from 'UI/App/App.react'

import es6Promise from 'es6-promise'
import injectTapEventPlugin from 'react-tap-event-plugin'

es6Promise.polyfill()
injectTapEventPlugin()

export default (
    <I18nextProvider i18n={i18n}>
        <Provider store={store}>
            <Router history={createBrowserHistory()}>
                <Route path="/" component={App}/>
            </Router>
        </Provider>
    </I18nextProvider>
)
