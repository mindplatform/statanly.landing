import './Services.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import Header from 'UI/Components/Header/Header.react'
import Footer from 'UI/Components/Footer/Footer.react'

import ServicesComponent from 'UI/Components/Services/Services.react'

export default class Services extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'Services'
        this._b = _b(this.boxClassName)
    }

    componentDidMount () {
        window.scrollTo(0,0)
    }

    render () {
        return (
            <section className={this._b}>
                <Header/>
                <ServicesComponent/>
                <Footer/>
            </section>
        )
    }
}