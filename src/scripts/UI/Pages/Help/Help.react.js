import './Help.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Header from 'UI/Components/Header/Header.react'
import ContactsComponent from 'UI/Components/Contacts/Contacts.react'
import Footer from 'UI/Components/Footer/Footer.react'

import * as InteractiveFeedbackActions from 'Redux/Actions/Interactive/Feedback.actions'
import RemoteRequestsStatusesSelector from 'Redux/Selectors/Statuses/RemoteRequestsStatuses.selector'

import {
    REMOTE_INTERACTIVE_FEEDBACK_SUCCESS,
    REMOTE_INTERACTIVE_FEEDBACK_FAILURE,
} from 'Redux/Actions/Interactive/Feedback.actions'
import EventMaster from 'Helpers/EventMaster/EventMaster.helper'

class Help extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'Help'
        this._b = _b(this.boxClassName)

        this.state = {
            feedBackSucceeded: false,
            feedBackFailure: false,
        }

        this.handleFeedBackSuccess = ::this.handleFeedBackSuccess
        this.handleFeedBackFailure = ::this.handleFeedBackFailure
    }

    handleFeedBackSuccess () {
        this.setState({
            feedBackSucceeded: true,
        })
    }

    handleFeedBackFailure () {
        this.setState({
            feedBackFailure: true,
        })
        setTimeout(() => this.setState({
            feedBackFailure: false,
        }), 2000)
    }

    componentDidMount () {
        window.scrollTo(0,0)
        EventMaster.addListener(REMOTE_INTERACTIVE_FEEDBACK_SUCCESS, this.handleFeedBackSuccess)
        EventMaster.addListener(REMOTE_INTERACTIVE_FEEDBACK_FAILURE, this.handleFeedBackFailure)
    }

    componentWillUnmount () {
        EventMaster.removeListener(REMOTE_INTERACTIVE_FEEDBACK_SUCCESS, this.handleFeedBackSuccess)
        EventMaster.removeListener(REMOTE_INTERACTIVE_FEEDBACK_FAILURE, this.handleFeedBackFailure)
    }

    render () {
        return (
            <section className={this._b}>
                <Header/>
                <ContactsComponent
                    onSubmit={this.props.InteractiveFeedbackActions.interactiveFeedback}
                    isInProcess={this.props.requestStates.isAnyRemoteRequestInProcess}
                    isSucceeded={this.state.feedBackSucceeded}
                    isFailure={this.state.feedBackFailure}
                />
                <Footer/>
            </section>
        )
    }
}

export default connect(
    state => ({
        requestStates: RemoteRequestsStatusesSelector({ state }),
    }),
    dispatch => ({
        InteractiveFeedbackActions: bindActionCreators(InteractiveFeedbackActions, dispatch),
    }),
)(Help)
