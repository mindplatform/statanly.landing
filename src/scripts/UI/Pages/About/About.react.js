import './About.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import Header from 'UI/Components/Header/Header.react'
import Footer from 'UI/Components/Footer/Footer.react'

import AboutComponent from 'UI/Components/About/About.react'

export default class About extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'About'
        this._b = _b(this.boxClassName)
    }

    componentDidMount () {
        window.scrollTo(0,0)
    }

    render () {
        return (
            <section className={this._b}>
                <Header/>
                <AboutComponent/>
                <Footer/>
            </section>
        )
    }
}