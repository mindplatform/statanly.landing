import './Home.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import Header from 'UI/Components/Header/Header.react'
import Splash from 'UI/Components/Splash/Splash.react'
import Technologies from 'UI/Components/Technologies/Technologies.react'

import QuestionBox from 'UI/Components/QuestionBox/QuestionBox.react'
import LinesOfBusiness from 'UI/Components/LinesOfBusiness/LinesOfBusiness.react'
import Slogan from 'UI/Components/Slogan/Slogan.react'
import Partners from 'UI/Components/Partners/Partners.react'
import PriceQuote from 'UI/Components/PriceQuote/PriceQuote.react'
import Footer from 'UI/Components/Footer/Footer.react'

export default class Home extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'Home'
        this._b = _b(this.boxClassName)
    }

    componentDidMount () {
        window.scrollTo(0,0)
    }

    render () {
        return (
            <section className={this._b}>
                <Header/>
                <Splash/>
                <Slogan/>
                <LinesOfBusiness/>
                <Technologies/>
                <QuestionBox/>
                <Partners/>
                <Footer/>
            </section>
        )
    }
}