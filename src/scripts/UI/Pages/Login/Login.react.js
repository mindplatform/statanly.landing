import './Login.less'
import React, { Component } from 'react'
import { Redirect } from 'react-router'
import _b from 'bem-cn'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Header from 'UI/Components/Header/Header.react'
import LoginComponent from 'UI/Components/Login/Login.react'
import Footer from 'UI/Components/Footer/Footer.react'

import * as AuthLoginActions from 'Redux/Actions/Auth/Login.actions'
import RemoteRequestsStatusesSelector from 'Redux/Selectors/Statuses/RemoteRequestsStatuses.selector'

import UserAccountSelector from 'Redux/Selectors/UserAccount/UserAccount.selector'

import {
    REMOTE_AUTH_LOGIN_FAILURE,
} from 'Redux/Actions/Auth/Login.actions'
import EventMaster from 'Helpers/EventMaster/EventMaster.helper'

class Login extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'Login'
        this._b = _b(this.boxClassName)

        this.state = {
            loginFailure: false,
        }

        this.handleLoginFailure = ::this.handleLoginFailure
    }

    handleLoginFailure () {
        this.setState({
            loginFailure: true,
        })
        setTimeout(() => this.setState({
            loginFailure: false,
        }), 2000)
    }

    componentDidMount () {
        window.scrollTo(0,0)
        EventMaster.addListener(REMOTE_AUTH_LOGIN_FAILURE, this.handleLoginFailure)
    }

    componentWillUnmount () {
        EventMaster.removeListener(REMOTE_AUTH_LOGIN_FAILURE, this.handleLoginFailure)
    }

    render () {
        const {
            isAuthenticated,
        } = this.props.userAccount

        if (isAuthenticated)
            return <Redirect to="/"/>

        return (
            <section className={this._b}>
                <Header/>
                <LoginComponent
                    onSubmit={this.props.AuthLoginActions.authLogin}
                    isInProcess={this.props.requestStates.isAnyRemoteRequestInProcess}
                    isFailure={this.state.loginFailure}
                />
                <Footer/>
            </section>
        )
    }
}

export default connect(
    state => ({
        requestStates: RemoteRequestsStatusesSelector({ state }),
        userAccount: UserAccountSelector({ state }),
    }),
    dispatch => ({
        AuthLoginActions: bindActionCreators(AuthLoginActions, dispatch),
    }),
)(Login)
