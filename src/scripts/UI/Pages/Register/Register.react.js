import './Register.less'
import React, { Component } from 'react'
import { Redirect } from 'react-router'
import _b from 'bem-cn'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Header from 'UI/Components/Header/Header.react'
import RegisterComponent from 'UI/Components/Register/Register.react'
import Footer from 'UI/Components/Footer/Footer.react'

import * as AuthRegisterActions from 'Redux/Actions/Auth/Register.actions'
import RemoteRequestsStatusesSelector from 'Redux/Selectors/Statuses/RemoteRequestsStatuses.selector'

import UserAccountSelector from 'Redux/Selectors/UserAccount/UserAccount.selector'

import {
    REMOTE_AUTH_REGISTER_FAILURE,
} from 'Redux/Actions/Auth/Register.actions'
import EventMaster from 'Helpers/EventMaster/EventMaster.helper'

class Register extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'Register'
        this._b = _b(this.boxClassName)

        this.state = {
            registerFailure: false,
        }

        this.handleRegisterFailure = ::this.handleRegisterFailure
    }

    handleRegisterFailure () {
        this.setState({
            registerFailure: true,
        })
        setTimeout(() => this.setState({
            registerFailure: false,
        }), 2000)
    }

    componentDidMount () {
        window.scrollTo(0,0)
        EventMaster.addListener(REMOTE_AUTH_REGISTER_FAILURE, this.handleRegisterFailure)
    }

    componentWillUnmount () {
        EventMaster.removeListener(REMOTE_AUTH_REGISTER_FAILURE, this.handleRegisterFailure)
    }

    render () {
        const {
            isAuthenticated,
        } = this.props.userAccount

        if (isAuthenticated)
            return <Redirect to="/"/>

        return (
            <section className={this._b}>
                <Header/>
                <RegisterComponent
                    onSubmit={this.props.AuthRegisterActions.authRegister}
                    isInProcess={this.props.requestStates.isAnyRemoteRequestInProcess}
                    isFailure={this.state.registerFailure}
                />
                <Footer/>
            </section>
        )
    }
}

export default connect(
    state => ({
        requestStates: RemoteRequestsStatusesSelector({ state }),
        userAccount: UserAccountSelector({ state }),
    }),
    dispatch => ({
        AuthRegisterActions: bindActionCreators(AuthRegisterActions, dispatch),
    }),
)(Register)
