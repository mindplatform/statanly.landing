import './Research.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import Header from 'UI/Components/Header/Header.react'
import Footer from 'UI/Components/Footer/Footer.react'

import ResearchComponent from 'UI/Components/Research/Research.react'

export default class Research extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'Research'
        this._b = _b(this.boxClassName)
    }

    componentDidMount () {
        window.scrollTo(0,0)
    }

    render () {
        return (
            <section className={this._b}>
                <Header/>
                <ResearchComponent/>
                <Footer/>
            </section>
        )
    }
}