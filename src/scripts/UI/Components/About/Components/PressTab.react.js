import './PressTab.less'
import React, { Component } from 'react'
import _b from 'bem-cn'
import { Link } from 'react-router-dom'

import { 
    translate,
} from 'react-i18next'

class PressTab extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'PressTab'
        this._b = _b(this.boxClassName)
    }

    renderLinkItemType (description, link, index) {
        return <a key={index + 'linkType'} target="_blank" href={link} className={this._b('ListBox')('List')('Item')('Body')('DescriptionLinkItem')}>{description}</a>
    }

    renderListItemType (description, index) {
        return <p key={index + 'listType'}  className={this._b('ListBox')('List')('Item')('Body')('DescriptionListItem')}>{description}</p>
    }

    renderDescriptionType (description, index) {
        return <div key={index + 'descriptionType'} className={this._b('ListBox')('List')('Item')('Body')('Description')}>{description}</div>
    }

    renderDescription (description) {
        if (description.map) {
            return (
                <div className={this._b('ListBox')('List')('Item')('Body')('DescriptionList')}>
                    {
                        description.map((v, index) => {
                            if (v.type == 'description') {
                                return this.renderDescriptionType(v.content, index)
                            } else if (v.type == 'listItem') {
                                return this.renderListItemType(v.content, index)
                            } else if (v.type == 'linkItem') {
                                return this.renderLinkItemType(v.content, v.link, index)
                            }
                        })
                    }
                </div>
            )
        }
        return this.renderDescriptionType(description)
    }

    renderItem ({
        head,
        description,
    }, index) {
        return (
            <li
                key={index + 'service'}
                className={this._b('ListBox')('List')('Item')}>
                <div className={this._b('ListBox')('List')('Item')('Body')}>
                    <div className={this._b('ListBox')('List')('Item')('Body')('Head')}>
                        <span className={this._b('ListBox')('List')('Item')('Body')('Head')('Before')}>―</span>
                        <span className={this._b('ListBox')('List')('Item')('Body')('Head')('Center')}>{head}</span>
                        <span className={this._b('ListBox')('List')('Item')('Body')('Head')('After')}>―</span>
                    </div>
                    {this.renderDescription(description)}
                </div>
            </li>
        )
    }

    render () {
        const {
            t,
        } = this.props

        this.descriptionList = [
            {
                head: t('PressTab.Public links on the web'),
                description: [
                    {
                        type: 'linkItem',
                        content: t('PressTab.Public links on the web.List[0]'),
                        link: 'http://news.ifmo.ru/ru/startups_and_business/startup/news/6653/',
                    },
                    {
                        type: 'linkItem',
                        content: t('PressTab.Public links on the web.List[1]'),
                        link: 'http://news.ifmo.ru/ru/news/6831/',
                    },
                    {
                        type: 'linkItem',
                        content: t('PressTab.Public links on the web.List[2]'),
                        link: 'http://ingria-park.ru/news_technopark/aingria_a_new_direction/',
                    },
                    {
                        type: 'linkItem',
                        content: t('PressTab.Public links on the web.List[3]'),
                        link: 'https://m.metronews.ru/novosti/peterbourg/reviews/iskusstvennye-mozgi-iz-rossii-idut-na-eksport-1350392/',
                    },
                ],
            },
        ]

        return (
            <section className={this._b}>
                <div className={this._b('Title')}>
                    {t('PressTab.Title.Main')}
                </div>
                <div className={this._b('ListBox')}>
                    <ul className={this._b('ListBox')('List')}>
                        {this.descriptionList.map((item, index) => this.renderItem(item, index))}
                    </ul>
                </div>
            </section>
        )
    }
}

export default translate('translations')(PressTab)
