import './MissionTab.less'
import React, { Component } from 'react'
import _b from 'bem-cn'
import { Link } from 'react-router-dom'

import { 
    translate,
} from 'react-i18next'
import i18n from 'Localization/i18n'

class MissionTab extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'MissionTab'
        this._b = _b(this.boxClassName)
    }

    renderListItemType (description, index) {
        return <p key={index + 'listType'}  className={this._b('ListBox')('List')('Item')('Body')('DescriptionListItem')}>{description}</p>
    }

    renderDescriptionType (description, index) {
        return <div key={index + 'descriptionType'} className={this._b('ListBox')('List')('Item')('Body')('Description')}>{description}</div>
    }

    renderDescription (description) {
        if (description.map) {
            return (
                <div className={this._b('ListBox')('List')('Item')('Body')('DescriptionList')}>
                    {
                        description.map((v, index) => {
                            if (v.type == 'description') {
                                return this.renderDescriptionType(v.content, index)
                            } else if (v.type == 'listItem') {
                                return this.renderListItemType(v.content, index)
                            }
                        })
                    }
                </div>
            )
        }
        return this.renderDescriptionType(description)
    }

    renderItem ({
        head,
        description,
    }, index) {
        return (
            <li
                key={index + 'service'} 
                className={this._b('ListBox')('List')('Item')}>
                <div className={this._b('ListBox')('List')('Item')('Body')}>
                    <div className={this._b('ListBox')('List')('Item')('Body')('Head')}>
                        <span className={this._b('ListBox')('List')('Item')('Body')('Head')('Before')}>―</span>
                        <span className={this._b('ListBox')('List')('Item')('Body')('Head')('Center')}>{head}</span>
                        <span className={this._b('ListBox')('List')('Item')('Body')('Head')('After')}>―</span>
                    </div>
                    {this.renderDescription(description)}
                </div>
            </li>
        )
    }

    render () {
        const {
            t,
        } = this.props

        this.descriptionList = [
            {
                head: t('MissionTab.Machine learning'),
                description: i18n.language == 'en' 
                    ? [
                        {
                            type: 'description',
                            content: t('MissionTab.Machine learning.Description[0]'),
                        },
                        {
                            type: 'description',
                            content: t('MissionTab.Machine learning.Description[1]'),
                        },
                    ]
                    : [
                        {
                            type: 'description',
                            content: t('MissionTab.Machine learning.Description[0]'),
                        },
                        {
                            type: 'description',
                            content: t('MissionTab.Machine learning.Description[1]'),
                        },
                        {
                            type: 'description',
                            content: t('MissionTab.Machine learning.Description[2]'),
                        },
                        {
                            type: 'description',
                            content: t('MissionTab.Machine learning.Description[3]'),
                        },
                        {
                            type: 'description',
                            content: t('MissionTab.Machine learning.Description[4]'),
                        },
                    ],
            },
            {
                head: t('MissionTab.Challenges of big data for enterprises'),
                description: [
                    {
                        type: 'description',
                        content: t('MissionTab.Challenges of big data for enterprises.Description[0]'),
                    },
                    {
                        type: 'listItem',
                        content: t('MissionTab.Challenges of big data for enterprises.List[0]'),
                    },
                    {
                        type: 'listItem',
                        content: t('MissionTab.Challenges of big data for enterprises.List[1]'),
                    },
                    {
                        type: 'listItem',
                        content: t('MissionTab.Challenges of big data for enterprises.List[2]'),
                    },
                    {
                        type: 'listItem',
                        content: t('MissionTab.Challenges of big data for enterprises.List[3]'),
                    },
                    {
                        type: 'listItem',
                        content: t('MissionTab.Challenges of big data for enterprises.List[4]'),
                    },
                    {
                        type: 'listItem',
                        content: t('MissionTab.Challenges of big data for enterprises.List[5]'),
                    },

                    {
                        type: 'description',
                        content: t('MissionTab.Challenges of big data for enterprises.Description[1]'),
                    },
                ],
            },
            {
                head: t('MissionTab.Our goal'),
                description: t('MissionTab.Our goal.Description'),
            },
            {
                head: t('MissionTab.Science and technology'),
                description: t('MissionTab.Science and technology.Description'),
            },
        ]

        return (
            <section className={this._b}>
                <div className={this._b('Title')}>
                    {t('MissionTab.Title.Main')}
                </div>
                <div className={this._b('Intro')}>
                    {t('MissionTab.Description.Main')}
                </div>
                <div className={this._b('ListBox')}>
                    <ul className={this._b('ListBox')('List')}>
                        
                        {this.descriptionList.map((item, index) => this.renderItem(item, index))}

                    </ul>
                </div>
            </section>
        )
    }
}

export default translate('translations')(MissionTab)