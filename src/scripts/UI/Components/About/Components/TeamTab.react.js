import './TeamTab.less'
import React, { Component } from 'react'
import _b from 'bem-cn'
import { Link } from 'react-router-dom'

import { 
    translate,
} from 'react-i18next'

class TeamTab extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'TeamTab'
        this._b = _b(this.boxClassName)
    }

    renderListItemType (description, index) {
        return <p key={index + 'listType'}  className={this._b('ListBox')('List')('Item')('Body')('DescriptionListItem')}>{description}</p>
    }

    renderDescriptionType (description, index) {
        return <div key={index + 'descriptionType'} className={this._b('ListBox')('List')('Item')('Body')('Description')}>{description}</div>
    }

    renderDescription (description) {
        if (description.map) {
            return (
                <div className={this._b('ListBox')('List')('Item')('Body')('DescriptionList')}>
                    {
                        description.map((v, index) => {
                            if (v.type == 'description') {
                                return this.renderDescriptionType(v.content, index)
                            } else if (v.type == 'listItem') {
                                return this.renderListItemType(v.content, index)
                            }
                        })
                    }
                </div>
            )
        }
        return this.renderDescriptionType(description)
    }

    renderItem ({
        head,
        description,
    }, index) {
        return (
            <li
                key={index + 'service'} 
                className={this._b('ListBox')('List')('Item')}>
                <div className={this._b('ListBox')('List')('Item')('Body')}>
                    <div className={this._b('ListBox')('List')('Item')('Body')('Head')}>
                        <span className={this._b('ListBox')('List')('Item')('Body')('Head')('Before')}>―</span>
                        <span className={this._b('ListBox')('List')('Item')('Body')('Head')('Center')}>{head}</span>
                        <span className={this._b('ListBox')('List')('Item')('Body')('Head')('After')}>―</span>
                    </div>
                    {this.renderDescription(description)}
                </div>
            </li>
        )
    }

    render () {
        const {
            t,
        } = this.props

        this.descriptionList = [
            {
                head: t('TeamTab.We are experts in'),
                description: [
                    {
                        type: 'listItem',
                        content: t('TeamTab.We are experts in.Description[0]'),
                    },
                    {
                        type: 'listItem',
                        content: t('TeamTab.We are experts in.Description[1]'),
                    },
                    {
                        type: 'listItem',
                        content: t('TeamTab.We are experts in.Description[2]'),
                    },
                    {
                        type: 'listItem',
                        content: t('TeamTab.We are experts in.Description[3]'),
                    },
                    {
                        type: 'listItem',
                        content: t('TeamTab.We are experts in.Description[4]'),
                    },
                    {
                        type: 'listItem',
                        content: t('TeamTab.We are experts in.Description[5]'),
                    },
                    {
                        type: 'listItem',
                        content: t('TeamTab.We are experts in.Description[6]'),
                    },
                    {
                        type: 'listItem',
                        content: t('TeamTab.We are experts in.Description[7]'),
                    },
                ],
            },
        ]

        return (
            <section className={this._b}>
                <div className={this._b('Title')}>
                    {t('TeamTab.Title.Main')}
                </div>
                <div className={this._b('Team')}>
                    <img 
                        src="/content/spa/images/team/team.jpg"
                        className={this._b('Team')('Photo')}
                    />
                </div>
                <div className={this._b('Intro')}>
                    {t('TeamTab.Description.Main')}
                </div>
                <div className={this._b('ListBox')}>
                    <ul className={this._b('ListBox')('List')}>
                        
                        {this.descriptionList.map((item, index) => this.renderItem(item, index))}

                    </ul>
                </div>
            </section>
        )
    }
}

export default translate('translations')(TeamTab)