import './About.less'
import React, { Component } from 'react'
import _b from 'bem-cn'
import {
    Route,
    Link,
} from 'react-router-dom'
import {
    withRouter,
} from 'react-router'
import {
    translate,
} from 'react-i18next'

import CompanyTab from './Components/CompanyTab.react'
import TeamTab from './Components/TeamTab.react'
import MissionTab from './Components/MissionTab.react'
import PressTab from './Components/PressTab.react'
import CooperationTab from './Components/CooperationTab.react'

class About extends Component {
    constructor (...args) {
        super(...args)
        this.boxClassName = 'AboutComponent'
        this._b = _b(this.boxClassName)
    }

    componentDidMount () {
        if (
            ~this.props.location.pathname.indexOf('team') ||
            ~this.props.location.pathname.indexOf('mission') ||
            ~this.props.location.pathname.indexOf('press') ||
            ~this.props.location.pathname.indexOf('cooperation')
        )
            return

        this.props.history.replace(`${this.props.match.path}/company`)
    }

    renderNavItem (item, index) {
        return (
            <li
                key={index + 'about-nav-item'}
                className={this._b('Nav')('Item')}
            >
                <Link to={item.link} className={this._b('Nav')('Item')('Link')}>
                    {item.title}
                </Link>
            </li>
        )
    }

    renderNav () {
        const {
            t,
            match,
        } = this.props

        this.navList = [
            {
                link: `${match.path}/company`,
                title: t('About.Nav.Company'),
            },
            {
                link: `${match.path}/team`,
                title: t('About.Nav.Team'),
            },
            {
                link: `${match.path}/mission`,
                title: t('About.Nav.Mission'),
            },
            {
                link: `${match.path}/press`,
                title: t('About.Nav.Press'),
            },
            {
                link: `${match.path}/cooperation`,
                title: t('About.Nav.Cooperation'),
            },
        ]

        return (
            <ul className={this._b('Nav')}>
                {this.navList.map((...args) => this.renderNavItem(...args))}
            </ul>
        )
    }

    render () {
        return (
            <section className={this._b}>
                {this.renderNav()}
                <Route path={`${this.props.match.path}/company`} component={CompanyTab}/>
                <Route path={`${this.props.match.path}/team`} component={TeamTab}/>
                <Route path={`${this.props.match.path}/mission`} component={MissionTab}/>
                <Route path={`${this.props.match.path}/press`} component={PressTab}/>
                <Route path={`${this.props.match.path}/cooperation`} component={CooperationTab}/>
            </section>
        )
    }
}

export default withRouter(translate('translations')(About))
