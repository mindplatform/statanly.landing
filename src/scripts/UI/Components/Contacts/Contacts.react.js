import './Contacts.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import FA from 'react-fontawesome'
import ContactForm from './ContactForm.react'

import {
    translate,
} from 'react-i18next'

class Contacts extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'ContactsComponent'
        this._b = _b(this.boxClassName)
    }

    render () {
        const {
            t,
        } = this.props

        return (
            <section className={this._b}>
                <div className={this._b('Box')}>
                    <div className={this._b('Body')}>
                        <h1 className={this._b('Box')('Body')('Title')}>
                            {t('Contact.Easy get in touch!')}
                        </h1>
                        <ul className={this._b('Box')('Body')('List')}>
                            <li className={this._b('Box')('Body')('List')('Item')}>
                                <FA name="location-arrow" className={this._b('Box')('Body')('List')('Item')('Icon').toString()}/>
                                {t('Contact.Saint Petersburg')}
                            </li>
                            <li className={this._b('Box')('Body')('List')('Item')}>
                                <a className={this._b('Box')('Body')('List')('Item')('Link')} href="tel:+7-921-875-23-96">
                                    <FA name="phone" className={this._b('Box')('Body')('List')('Item')('Icon').toString()}/>
                                    +7 921 875 23 96
                                </a>
                            </li>
                            <li className={this._b('Box')('Body')('List')('Item')}>
                                <a className={this._b('Box')('Body')('List')('Item')('Link')} href="mailto:hello@statanly.com">
                                    <FA name="envelope-o" className={this._b('Box')('Body')('List')('Item')('Icon').toString()}/>
                                    hello@statanly.com
                                </a>
                            </li>
                            <li className={this._b('Box')('Body')('List')('Item')}>
                                <a className={this._b('Box')('Body')('List')('Item')('Link')} href="https://www.facebook.com/statanly" target="_blank">
                                    <FA name="facebook-f" className={this._b('Box')('Body')('List')('Item')('Icon').toString()}/>
                                    @statanly
                                </a>
                            </li>
                            <li className={this._b('Box')('Body')('List')('Item')}>
                                <a className={this._b('Box')('Body')('List')('Item')('Link')} href="https://twitter.com/statanly" target="_blank">
                                    <FA name="twitter" className={this._b('Box')('Body')('List')('Item')('Icon').toString()}/>
                                    @statanly
                                </a>
                            </li>
                            <li className={this._b('Box')('Body')('List')('Item')}>
                                <a className={this._b('Box')('Body')('List')('Item')('Link')} href="https://www.instagram.com/statanly" target="_blank">
                                    <FA name="instagram" className={this._b('Box')('Body')('List')('Item')('Icon').toString()}/>
                                    @statanly
                                </a>
                            </li>
                        </ul>
                        <ContactForm
                            {...this.props}
                        />
                    </div>
                </div>
            </section>
        )
    }
}

export default translate('translations')(Contacts)
