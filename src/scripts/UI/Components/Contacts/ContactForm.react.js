import './ContactForm.less'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import _b from 'bem-cn'

import FA from 'react-fontawesome'

import {
    translate,
} from 'react-i18next'

class ContactForm extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'ContactForm'
        this._b = _b(this.boxClassName)

        this.handleSubmit = ::this.handleSubmit
    }

    handleSubmit (event) {
        const {
            target,
            nativeEvent,
        } = event

        const {
            isInProcess,
        } = this.props

        if (!isInProcess && target.checkValidity())
            this.props.onSubmit({
                body: new FormData(target),
            })

        nativeEvent.preventDefault()
    }

    renderSubmitButton () {
        const {
            t,
            isInProcess,
        } = this.props

        if (isInProcess)
            return <FA name="spinner" spin/>

        return t('Contact.Send')
    }

    renderFooter () {
        const {
            t,
            isSucceeded,
            isFailure,
        } = this.props

        if (isSucceeded) {
            return (
                <div className={this._b('Successfully')}>
                    {t('Contact.Your message has been sent')}
                </div>
            )

        } else {
            if (isFailure) {
                return (
                    <div className={this._b('Error')}>
                        {t('Auth.Error')}
                    </div>
                )
            } else {
                return (
                    <button className={this._b('Button')}>
                        {this.renderSubmitButton()}
                    </button>
                )
            }
        }
    }

    render () {
        const {
            t,
            isInProcess,
            isSucceeded,
        } = this.props

        const isDisabled = isSucceeded || isInProcess

        return (
            <div className={this._b}>
                <h2 className={this._b('Title')}>{isSucceeded ? t('Contact.Thank you for contacting us!') : t('Contact.Contact Us')}</h2>
                <form onSubmit={this.handleSubmit}>
                    <input className={this._b('Input')} name="name" type="text" placeholder={t('Contact.Name')} required disabled={isDisabled}/>
                    <input className={this._b('Input')} name="email" type="email" placeholder={t('Contact.Email')} required disabled={isDisabled}/>
                    <textarea className={this._b('Textarea')} name="text" rows="3" placeholder={t('Contact.Your question')} required disabled={isDisabled}></textarea>
                    {this.renderFooter()}
                </form>
            </div>
        )
    }
}

ContactForm.propTypes = {
    onSubmit: PropTypes.func,
    isInProcess: PropTypes.bool,
    isSucceeded: PropTypes.bool,
    isFailure: PropTypes.bool,
}

ContactForm.defaultProps = {
    onSubmit: () => {},
    isInProcess: false,
    isSucceeded: false,
    isFailure: false,
}

export default translate('translations')(ContactForm)
