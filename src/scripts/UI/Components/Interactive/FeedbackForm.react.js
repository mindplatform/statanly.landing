import './FeedbackForm.less'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import _b from 'bem-cn'

import FA from 'react-fontawesome'

import {
    translate,
} from 'react-i18next'

class FeedbackForm extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'FeedbackForm'
        this._b = _b(this.boxClassName)

        this.state = {
            formEnabled: false,
        }

        this.handleSubmit = ::this.handleSubmit
        this.handleFormOn = ::this.handleFormOn
    }

    handleFormOn() {
        this.setState({
            formEnabled: true,
        })
    }

    handleSubmit (event) {
        const {
            target,
            nativeEvent,
        } = event

        const {
            isInProcess,
        } = this.props

        if (!isInProcess && target.checkValidity())
            this.props.onSubmit({
                body: new FormData(target),
            })

        nativeEvent.preventDefault()
    }

    renderSubmitButton () {
        const {
            t,
            isInProcess,
        } = this.props

        if (isInProcess)
            return <FA name="spinner" spin/>

        return t('Contact.Send')
    }

    renderFooter () {
        const {
            t,
            isSucceeded,
            isFailure,
        } = this.props

        if (isSucceeded) {
            return (
                <div className={this._b('Successfully')}>
                    {t('Contact.Your message has been sent')}
                </div>
            )

        } else {
            if (isFailure) {
                return (
                    <div className={this._b('Error')}>
                        {t('Auth.Error')}
                    </div>
                )
            } else {
                return (
                    <button className={this._b('Button')}>
                        {this.renderSubmitButton()}
                    </button>
                )
            }
        }
    }

    render () {
        const {
            t,
            isInProcess,
            isSucceeded,
        } = this.props

        const isDisabled = isSucceeded || isInProcess

        return (
            <div className={this._b}>
                <h2
                    className={this._b('Title')}
                    onClick={this.handleFormOn}
                >
                    <div className={this._b('Title')('Inner').mix([this.state.formEnabled ? '' : 'asButton'])}>
                        {isSucceeded
                            ? t('FeedbackForm.Thank you for contacting us!')
                            : this.state.formEnabled
                                ? t('FeedbackForm.Contact Us')
                                : t('FeedbackForm.Contact Us (as button)')
                        }
                    </div>
                </h2>
                <form onSubmit={this.handleSubmit} className={this._b('Form').mix([this.state.formEnabled ? 'active' : 'inactive'])}>
                    <input className={this._b('Input')} name="name" type="text" placeholder={t('FeedbackForm.Name')} required disabled={isDisabled}/>
                    <input className={this._b('Input')} name="email" type="email" placeholder={t('FeedbackForm.Email')} required disabled={isDisabled}/>
                    <textarea className={this._b('Textarea')} name="text" rows="3" placeholder={t('FeedbackForm.Your question')} required disabled={isDisabled}></textarea>
                    {this.renderFooter()}
                </form>
            </div>
        )
    }
}

FeedbackForm.propTypes = {
    onSubmit: PropTypes.func,
    isInProcess: PropTypes.bool,
    isSucceeded: PropTypes.bool,
    isFailure: PropTypes.bool,
}

FeedbackForm.defaultProps = {
    onSubmit: () => {},
    isInProcess: false,
    isSucceeded: false,
    isFailure: false,
}

export default translate('translations')(FeedbackForm)
