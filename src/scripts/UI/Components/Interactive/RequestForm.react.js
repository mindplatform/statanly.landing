import './RequestForm.less'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import _b from 'bem-cn'

import FA from 'react-fontawesome'

import {
    translate,
} from 'react-i18next'

class RequestForm extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'RequestForm'
        this._b = _b(this.boxClassName)

        this.state = {
            formEnabled: false,
        }

        this.handleSubmit = ::this.handleSubmit
        this.handleFormOn = ::this.handleFormOn
    }

    handleFormOn() {
        this.setState({
            formEnabled: true,
        })
    }

    handleSubmit (event) {
        const {
            target,
            nativeEvent,
        } = event

        const {
            isInProcess,
        } = this.props

        if (!isInProcess && target.checkValidity())
            this.props.onSubmit({
                body: new FormData(target),
            })

        nativeEvent.preventDefault()
    }

    renderSubmitButton () {
        const {
            t,
            isInProcess,
        } = this.props

        if (isInProcess)
            return <FA name="spinner" spin/>

        return t('Contact.Send')
    }

    renderFooter () {
        const {
            t,
            isSucceeded,
            isFailure,
        } = this.props

        if (isSucceeded) {
            return (
                <div className={this._b('Successfully')}>
                    {t('Contact.Your message has been sent')}
                </div>
            )

        } else {
            if (isFailure) {
                return (
                    <div className={this._b('Error')}>
                        {t('Auth.Error')}
                    </div>
                )
            } else {
                return (
                    <button className={this._b('Button')}>
                        {this.renderSubmitButton()}
                    </button>
                )
            }
        }
    }

    render () {
        const {
            t,
            isInProcess,
            isSucceeded,
        } = this.props

        const isDisabled = isSucceeded || isInProcess

        return (
            <div className={this._b}>
                <h2 className={this._b('Title')}
                    className={this._b('Title')}
                    onClick={this.handleFormOn}
                >
                    <div className={this._b('Title')('Inner').mix([this.state.formEnabled ? '' : 'asButton'])}>
                        {isSucceeded
                            ? t('RequestForm.Thank you for requesting us!')
                            : this.state.formEnabled
                                ? t('RequestForm.Contact Us')
                                : t('RequestForm.Contact Us (as button)')
                        }
                    </div>
                </h2>
                <form onSubmit={this.handleSubmit} className={this._b('Form').mix([this.state.formEnabled ? 'active' : 'inactive'])}>
                    <input className={this._b('Input')} name="name" type="text" placeholder={t('RequestForm.Name')} required disabled={isDisabled}/>
                    <input className={this._b('Input')} name="email" type="email" placeholder={t('RequestForm.Email')} required disabled={isDisabled}/>
                    <input className={this._b('Input')} name="phone" type="tel" placeholder={t('RequestForm.Phone')} disabled={isDisabled}/>
                    <label className={this._b('TopicLabel')} htmlFor="contactTopicSelect">{t('RequestForm.Select.TopicLabel')}</label>
                    <select id="contactTopicSelect" className={this._b('Select')} name="subject" required disabled={isDisabled}>
                        <option value="прогнозирование">{t('RequestForm.Select.Forecasting')}</option>
                        <option value="рекомендательная система">{t('RequestForm.Select.RecommendationSystems')}</option>
                        <option value="классификация">{t('RequestForm.Select.Classification')}</option>
                        <option value="сегментация и кластеризация">{t('RequestForm.Select.SegmentationAndClustering')}</option>
                        <option value="другое">{t('RequestForm.Select.Other')}</option>
                    </select>
                    <textarea className={this._b('Textarea')} name="text" rows="3" placeholder={t('RequestForm.Your question')} required disabled={isDisabled}></textarea>
                    {this.renderFooter()}
                </form>
            </div>
        )
    }
}

RequestForm.propTypes = {
    onSubmit: PropTypes.func,
    isInProcess: PropTypes.bool,
    isSucceeded: PropTypes.bool,
    isFailure: PropTypes.bool,
}

RequestForm.defaultProps = {
    onSubmit: () => {},
    isInProcess: false,
    isSucceeded: false,
    isFailure: false,
}

export default translate('translations')(RequestForm)
