import './PriceQuote.less'
import React, { Component } from 'react'

import bem from 'Helpers/Bem/Bem.helper'
import classNames from 'classnames'

import {
    Link,
} from 'react-router-dom'

import {
    translate,
} from 'react-i18next'

class PriceQuote extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'PriceQuote'
        this.bem = bem.with(this.boxClassName)
    }

    render () {
        const {
            t,
        } = this.props

        return (
            <div className={this.bem()}>
                <div className={this.bem('Title')}>
                    <div className={this.bem('Title-H1')}>
                        Коммерческое предложение
                    </div>
                    <div className={this.bem('Title-H2')}>
                        Каждый из перечисленных этапов разработки является законченным решением и может быть использован независимо от остальных. В процессе разработки мы используем самые современные технологии и решения. В зависимости от пожеланий заказчика выбор стека технологий, языков программирования используемый для разработки может обсуждаться отдельно.
                    </div>
                </div>
                <div className={this.bem("Box")}>
                    <div
                        className={this.bem("QuoteItem")}
                        style={{
                            backgroundColor: "#81d2e4",
                            color: "#0c3841",
                        }}
                    >
                        <div className={this.bem("QuoteItem-Header")}>Первичный анализ данных, аналитика, постановка задачи, разработка технического задания</div>
                        <div className={this.bem("QuoteItem-Body")}>На основе представлений заказчика о задаче, которую необходимо решить, производится экспертное исследование. По итогам его проведения разрабатывается техническое задание, в котором  задача уточняется либо ставится новая, а также формализуются требования к проводимому далее исследованию. Кроме того, заказчику может быть предложено собрать дополнительные данные или уточнить уже существующие с целью улучшения качества получаемых впоследствии прогнозных моделей.</div>
                        <div className={this.bem("QuoteItem-Footer")}>Сроки: 2 — 4 недели</div>
                    </div>
                    <div
                        className={this.bem("QuoteItem")}
                        style={{
                            backgroundColor: "#af98f4",
                            color: "#221352",
                        }}
                    >
                        <div className={this.bem("QuoteItem-Header")}>Разработка математических моделей на основании поставленного ТЗ</div>
                        <div className={this.bem("QuoteItem-Body")}>На основании технического задания производится разработка, обучение и тонкая настройка математических (прогнозных) моделей.  Результатом разработки является программный код, а также документация для последующего внедрения в любые инфраструктуры и приложения.</div>
                        <div className={this.bem("QuoteItem-Footer")}>Сроки: 2 — 4 недели</div>
                    </div>
                    <div
                        className={this.bem("QuoteItem")}
                        style={{
                            backgroundColor: "#8cd34c",
                            color: "#1e3906",
                        }}
                    >
                        <div className={this.bem("QuoteItem-Header")}>Внедрение разработанных алгоритмов</div>
                        <div className={this.bem("QuoteItem-Body")}>Внедрение разработанных алгоритмов в инфраструктуры корпоративных приложений заказчика. Результатом разработки является создание:</div>
                        <ul className={this.bem("QuoteItem-List")}>
                            <li className={this.bem("QuoteItem-List-Item")}>веб-приложений</li>
                            <li className={this.bem("QuoteItem-List-Item")}>мобильных приложений</li>
                            <li className={this.bem("QuoteItem-List-Item")}>десктопных приложений</li>
                            <li className={this.bem("QuoteItem-List-Item")}>серверного API</li>
                        </ul>
                        <div className={this.bem("QuoteItem-Footer")}>Сроки: 4 — 8 недель</div>
                    </div>
                </div>
            </div>
        )
    }
}

export default translate('translations')(PriceQuote)
