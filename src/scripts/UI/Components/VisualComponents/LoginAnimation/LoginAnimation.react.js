import './LoginAnimation.less'
import D3LoginAnimation from './LoginAnimation.d3'
import React, { Component } from 'react'
import _b from 'bem-cn'

export default class LoginAnimation extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'LoginAnimation'
        this._b = _b(this.boxClassName)

        this.state = {
            width: 0,
            height: 0,
        }
    }

    componentDidMount () {
        const rect = this.refBody.getBoundingClientRect()

        const cluster = new D3LoginAnimation({
            canvas: this.refCanvas,
            width: rect.width,
            height: rect.height,
        })

        this.setState({
            width: rect.width,
            height: rect.height,
        })

        cluster.start()
    }

    render () {
        const {
            className,
        } = this.props
        
        return (
            <section 
                ref={ref => this.refBody = ref} 
                className={this._b.mix(className)}>
                <canvas 
                    ref={ref => this.refCanvas = ref} 
                    className={this._b('Canvas')}

                    width={this.state.width}
                    height={this.state.height}
                />
            </section>
        )
    }
}