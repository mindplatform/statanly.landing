import * as d3 from 'd3'
import * as d3ScaleChromatic from 'd3-scale-chromatic'

export default class LoginAnimation {
    constructor ({
        canvas,
        width,
        height,
    }) {
        this.width = width
        this.height = height
        this.context = canvas.getContext("2d")
        this.particles = []
        this.tau = 2 * Math.PI

        this.cluster = d3.scaleQuantize()
            .domain([0, 1])
            .range([1, 2, 3])

        for (var i = 0; i < 300; i++) {
            this.particles.push(this.createParticle())
        }
    }

    createParticle () {
        const clusterRandom = Math.random()
        return {
            id: Math.floor(clusterRandom*1e1),
            x: Math.random() > .5 ? 0 - Math.random() * 10 : this.width + Math.random() * 10,
            y: Math.random() > .5 ? 0 - Math.random() * 10 : this.height + Math.random() * 10,
            r: .1,
            cluster: this.cluster(clusterRandom),
            color: d3ScaleChromatic.interpolateBlues(clusterRandom),
        }
    }

    start () {
        const isFirst = !this.simulation
        this.simulation = this.simulation || d3.forceSimulation(this.particles)
            .velocityDecay(.7)
            .force("forceX", d3.forceX().strength(.002))
            .force("forceY", d3.forceY().strength(.002))
            .alphaTarget(1)
            .on("tick", () => this.ticked())
            
        this.simulation
            .force("center", d3.forceCenter())
            .force("collide", d3.forceCollide().radius(d => d.r + 1.5).iterations(2))
            .force("charge", d3.forceManyBody().strength(-25))


        this.simulation
            .force("forceX", d3.forceX().strength(.1))
            .force("forceY", d3.forceY().strength(.1))
            .force("center", d3.forceCenter())

        this.step()
    }

    // step2 () {
    //     const duration = 1500
    //     const ease = d3.easeCubic

    //     const newRadius = {}
    //     this.particles.forEach((particle, index) => {
    //         if (index % 20 == 0) {
    //             newRadius[index] = Math.random()*1e2 / 2
    //         }
    //     })

    //     this.timer1 = d3.timer(elapsed => {
    //         // compute how far through the animation we are (0 to 1)
    //         const t = Math.min(1, ease(elapsed / duration))

    //         // update point positions (interpolate between source and target)
    //         this.particles.map((particle, index) => {
    //             if (index % 20 == 0) {
    //                 particle.r = Math.random()*1e1 * (1 - t) + newRadius[index] * t
    //             }
    //             return particle
    //         })

    //         this.simulation
    //             .force("collide", d3.forceCollide().radius(d => d.r + 1.5).iterations(2))

    //         // if this animation is over
    //         if (t === 1) {
    //             // stop this timer since we are done animating.
    //             this.timer1.stop()
    //             setTimeout(() => this.step3i(), 3000)
    //         }
    //     })
    // }

    // step3i () {
    //     const duration = 1500
    //     const ease = d3.easeCubic

    //     // const newRadius = {}
    //     // this.particles.forEach((particle, index) => {
    //     //     if (index % 20 == 0) {
    //     //         newRadius[index] = Math.random()*1e1
    //     //     }
    //     // })

    //     const mem = this.particles.map(particle => ({
    //         x: particle.x,
    //         y: particle.y,
    //     }))

    //     this.timer = d3.timer(elapsed => {
    //         // compute how far through the animation we are (0 to 1)
    //         const t = Math.min(1, ease(elapsed / duration))

    //         // update point positions (interpolate between source and target)
    //         this.particles.map((particle, index) => {
    //             if (index % 20 == 0) {
    //                 particle.x = mem[index].x * (1 - t) + this.width/4 * t
    //                 particle.y = mem[index].y * (1 - t) + this.height/2 * t
    //             } else {
    //                 particle.x = mem[index].x * (1 - t) + this.width/3 * t
    //                 particle.y = mem[index].y * (1 - t) + this.height/2 * t
    //             }
    //             return particle
    //         })

    //         this.simulation
    //             .force("collide", d3.forceCollide().strength(.01).radius(d => d.r + 1e2).iterations(2))
    //             .force("charge", d3.forceManyBody().strength(-7))

    //         // if this animation is over
    //         if (t === 1) {
    //             // stop this timer since we are done animating.
    //             this.timer.stop()
    //             setTimeout(() => this.step3(), 3000)
    //         }
    //     })
    // }

    // step3 () {
    //     const duration = 1500
    //     const ease = d3.easeCubic

    //     const newRadius = {}
    //     this.particles.forEach((particle, index) => {
    //         if (index % 20 == 0) {
    //             newRadius[index] = Math.random()*1e1
    //         }
    //     })

    //     this.timer2 = d3.timer(elapsed => {
    //         // compute how far through the animation we are (0 to 1)
    //         const t = Math.min(1, ease(elapsed / duration))

    //         // update point positions (interpolate between source and target)
    //         this.particles.map((particle, index) => {
    //             if (index % 20 == 0) {
    //                 particle.r = particle.r * (1 - t) + newRadius[index] * t
    //             }
    //             return particle
    //         })

    //         const nest = d3.nest().key(d => d.id).entries(this.particles)
    //         const groupsScale = d3.scaleThreshold().domain(d3.range(nest.length).slice(1).map(d => d/1e1)).range(d3.range(nest.length))
    //         this.links = nest.map((group, index, list) => group.values.map(value => {
    //             const targetGroupIndex = groupsScale(Math.random())
    //             const targetGroup = nest[targetGroupIndex]
    //             const targetGroupScale = d3.scaleThreshold().domain(d3.range(targetGroup.values.length).slice(1).map(d => d/1e1)).range(d3.range(targetGroup.values.length))

    //             const color = d3.color(value.color)
    //             color.opacity = Math.random() / 2

    //             return {
    //                 source: value,
    //                 target: targetGroup.values[targetGroupScale(Math.random())],
    //                 color,
    //             }
    //         })).reduce((memo, group) => memo.concat(group), []).slice(0, 5)

    //         this.simulation
    //             .force("collide", d3.forceCollide().radius(d => d.r + 1.5).iterations(2))
    //             .force("charge", d3.forceManyBody().strength(-100))

    //         // if this animation is over
    //         if (t === 1) {
    //             // stop this timer since we are done animating.
    //             this.timer2.stop()
    //             this.links = null
    //             setTimeout(() => this.step4i(), 500)
    //         }
    //     })
    // }

    // step4i () {
    //     const duration = 1500
    //     const ease = d3.easeCubic

    //     const xNew = this.particles.map(d => d3.quantile([-100, this.width + 100], Math.random()))
    //     const yNew = this.particles.map(d => d3.quantile([-100, this.height + 100], Math.random()))
        
    //     this.timer5 = d3.timer(elapsed => {

    //         const t = Math.min(1, ease(elapsed / duration))

    //         this.particles = this.particles.map((particle, index) => {
    //             particle.x = particle.x * (1 - t) + xNew[index] * t
    //             particle.y = particle.y * (1 - t) + yNew[index] * t
    //             return particle
    //         })

    //         this.simulation
    //             .nodes(this.particles)
    //             .force("forceX", d3.forceX().strength(.001))
    //             .force("forceY", d3.forceY().strength(.001))
    //             .force("center", d3.forceCenter())
    //             .force("collide", d3.forceCollide().radius(d => d.r + 1.5).iterations(2))
    //             .force("charge", d3.forceManyBody().strength(-100))

    //         if (t === 1) {
    //             // stop this timer since we are done animating.
    //             this.timer5.stop()
    //             setTimeout(() => this.step4(), 3000)
    //         }
    //     })
    // }

    // step4 () {
    //     const duration = 1500
    //     const ease = d3.easeCubic

    //     const x = d3.scaleLinear().rangeRound([this.width/4, 3*this.width/4])
    //     x.domain(d3.extent(this.particles, d => d.x))

    //     const y = d3.scaleLinear().rangeRound([this.height/4, 3*this.height/4])
    //     y.domain(d3.extent(this.particles, d => d.r))

    //     const mem = this.particles.map(particle => ({
    //         x: x(particle.x),
    //         y: y(particle.r),
    //     }))

    //     this.timer3 = d3.timer(elapsed => {

    //         const t = Math.min(1, ease(elapsed / duration))

    //         this.particles.map((particle, index) => {
    //             particle.x = particle.x * (1 - t) + mem[index].x * t
    //             particle.y = particle.y * (1 - t) - mem[index].y * t
    //             return particle
    //         })

    //         this.simulation
    //             .force("forceX", d => d3.forceX(d.x).strength(.1))
    //             .force("forceY", d => d3.forceY(d.y).strength(.1))
    //             .force("collide", null)

    //         if (t === 1) {
    //             // stop this timer since we are done animating.
    //             this.timer3.stop()
    //             this.simulation.stop()
    //             setTimeout(() => this.simulation.restart(), 1500)
    //             setTimeout(() => this.step5(), 4000)
    //         }
    //     })
    // }

    // step5 () {
    //     const duration = 1500
    //     const ease = d3.easeCubic
        
    //     this.timer4 = d3.timer(elapsed => {

    //         const t = Math.min(1, ease(elapsed / duration))

    //         const nodes = this.particles.map((particle, index) => {
    //             if (particle.cluster == 1) {
    //                 particle.x = particle.x * (1 - t) + this.width/4 * t
    //                 particle.y = particle.y * (1 - t) + this.height/2 * t
                
    //             } else if (particle.cluster == 2) {
    //                 particle.x = particle.x * (1 - t) + this.width/2 * t
    //                 particle.y = particle.y * (1 - t) + this.height/2 * t

    //             } else {
    //                 particle.x = particle.x * (1 - t) + 3 * this.width/4 * t
    //                 particle.y = particle.y * (1 - t) + this.height/2 * t
    //             }

    //             return particle
    //         })

    //         this.simulation
    //             .nodes(nodes)
    //             .force("collide", d3.forceCollide().strength(.1).radius(d => d.r + 1.5).iterations(2))
    //             // .force("center", d3.forceCenter())
    //             .force("charge", d3.forceManyBody().strength(-1))

    //         if (t === 1) {
    //             // stop this timer since we are done animating.
    //             this.timer4.stop()
    //             setTimeout(() => this.step6(), 3000)
    //         }
    //     })
    // }

    // step6 () {
    //     const duration = 1500
    //     const ease = d3.easeCubic

    //     const xNew = this.particles.map(d => d3.quantile([-100, this.width + 100], Math.random()))
    //     const yNew = this.particles.map(d => d3.quantile([-100, this.height + 100], Math.random()))
        
    //     this.timer5 = d3.timer(elapsed => {

    //         const t = Math.min(1, ease(elapsed / duration))

    //         this.particles = this.particles.map((particle, index) => {
    //             particle.x = particle.x * (1 - t) + xNew[index] * t
    //             particle.y = particle.y * (1 - t) + yNew[index] * t
    //             return particle
    //         })

    //         this.simulation
    //             .nodes(this.particles)
    //             .force("forceX", d3.forceX().strength(.001))
    //             .force("forceY", d3.forceY().strength(.001))
    //             .force("center", d3.forceCenter())
    //             .force("collide", d3.forceCollide().radius(d => d.r + 1.5).iterations(2))
    //             .force("charge", d3.forceManyBody().strength(-100))

    //         if (t === 1) {
    //             // stop this timer since we are done animating.
    //             this.timer5.stop()
    //             setTimeout(() => this.step7(), 3000)
    //         }
    //     })
    // }

    step () {
        const duration = 1500
        const ease = d3.easeCubic

        const nest = d3.nest().key(d => d.id).entries(this.particles)
        const groupsScale = d3.scaleThreshold().domain(d3.range(nest.length).slice(1).map(d => d/1e1)).range(d3.range(nest.length))
        this.links = nest.map((group, index, list) => group.values.map(value => {
            const targetGroupIndex = groupsScale(Math.random())
            const targetGroup = nest[targetGroupIndex]
            const targetGroupScale = d3.scaleThreshold().domain(d3.range(targetGroup.values.length).slice(1).map(d => d/1e1)).range(d3.range(targetGroup.values.length))

            const color = d3.color(value.color)
            color.opacity = .2

            return {
                source: value,
                target: targetGroup.values[targetGroupScale(Math.random())],
                color,
            }
        })).reduce((memo, group) => memo.concat(group), [])
        
        this.timer = d3.timer(elapsed => {

            const t = Math.min(1, ease(elapsed / duration))

            this.particles = this.particles.map((particle, index) => {
                particle.r = particle.r * (1 - t) + 1 * t
                return particle
            })

            this.simulation
                .force("forceX", d3.forceX().strength(.1))
                .force("forceY", d3.forceY().strength(.1))
                .force("link", d3.forceLink().distance(40))
                .force("collide", d3.forceCollide().radius(d => d.r + 1.5).iterations(2))
                .force("charge", d3.forceManyBody().strength(-200))

            if (t === 1) {
                // stop this timer since we are done animating.
                this.timer.stop()
                // setTimeout(() => this.start(), 3000)
            }
        })
    }

    // step () {
    //     const duration = 3000
    //     const ease = d3.easeCubic

    //     const newRadius = this.particles.map(particle => Math.random()*1e1)
        
    //     this.timer = d3.timer(elapsed => {

    //         const t = Math.min(1, ease(elapsed / duration))

    //         this.particles = this.particles.map((particle, index) => {
    //             particle.r = particle.r * (1 - t) + newRadius[index] * t
    //             return particle
    //         })

    //         if (this.links && this.links.length) {
    //             this.links = this.links.slice(0, -3)
    //         }

    //         this.simulation
    //             .force("collide", d3.forceCollide().radius(d => d.r + 1.5).iterations(2))

    //         if (this.links.length == 0) {
    //             // stop this timer since we are done animating.
    //             this.links = null
    //             this.timer.stop()
    //             setTimeout(() => this.start(), 1000)
    //         }
    //     })
    // }

    ticked () {
        this.context.clearRect(0, 0, this.width, this.height)
        this.context.save()
        this.context.translate(this.width / 2, this.height / 2)

        this.particles.forEach(d => {
            this.context.beginPath()

            this.context.moveTo(d.x + d.r, d.y)
            this.context.arc(d.x, d.y, d.r, 0, this.tau)

            this.context.fillStyle = d.color
            this.context.fill()

            this.context.strokeStyle = "#333"
            this.context.stroke()
        })

        this.links && this.links.forEach(d => {
            this.context.beginPath()
            
            this.context.strokeStyle = d.color

            this.context.moveTo(d.source.x, d.source.y)
            this.context.lineTo(d.target.x, d.target.y)

            this.context.stroke()
        })

        this.context.restore()
    }
}