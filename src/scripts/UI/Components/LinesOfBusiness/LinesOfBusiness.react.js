import './LinesOfBusiness.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import {
    Link,
} from 'react-router-dom'

import FeedbackForm from 'UI/Containers/Interactive/FeedbackForm.react'
import RequestForm from 'UI/Containers/Interactive/RequestForm.react'

import {
    translate,
} from 'react-i18next'

class LinesOfBusiness extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'LinesOfBusiness'
        this._b = _b(this.boxClassName)
    }

    render () {
        const {
            t,
        } = this.props

        return (
            <section className={this._b}>
                <div className={this._b('Lines')}>
                    <div className={this._b('Lines')('Line')}>
                        <div className={this._b('Lines')('Line')('Title')}>
                            <div className={this._b('Lines')('Line')('Title')('Inner')}>
                                {t('LinesOfBusiness.AutoML')}
                            </div>
                        </div>
                        <div className={this._b('Lines')('Line')('SubTitle')}>
                            {t('LinesOfBusiness.AutoML.SubTitle')}
                        </div>
                        <div className={this._b('Lines')('Line')('Description')}>
                            {t('LinesOfBusiness.AutoML.Description')}
                            <Link className={this._b('Lines')('Line')('More')} to="/services/howto">
                                {t('LinesOfBusiness.More')}
                            </Link>
                        </div>
                        <FeedbackForm/>
                    </div>
                    <div className={this._b('Lines')('Line')}>
                        <div className={this._b('Lines')('Line')('Title')}>
                            <div className={this._b('Lines')('Line')('Title')('Inner')}>
                                {t('LinesOfBusiness.Custom solutions')}
                            </div>
                        </div>
                        <div className={this._b('Lines')('Line')('SubTitle')}>
                            {t('LinesOfBusiness.Custom solutions.SubTitle')}
                        </div>
                        <div className={this._b('Lines')('Line')('Description')}>
                            {t('LinesOfBusiness.Custom solutions.Description')}
                            <Link className={this._b('Lines')('Line')('More')} to="/services/core">
                                {t('LinesOfBusiness.More')}
                            </Link>
                        </div>
                        <RequestForm/>
                    </div>
                </div>
            </section>
        )
    }
}

export default translate('translations')(LinesOfBusiness)
