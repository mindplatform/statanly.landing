import './Header.less'
import React, { Component } from 'react'
import _b from 'bem-cn'
import { Link } from 'react-router-dom'

import AuthAccount from 'UI/Containers/Auth/AuthAccount.react'
import AuthLoginButton from 'UI/Containers/Auth/AuthLoginButton.react'
import UserAccount from 'UI/Containers/UserAccount/UserAccount.react'

import {
    translate,
} from 'react-i18next'

class Header extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'Header'
        this._b = _b(this.boxClassName)

        this.state = {
            drawerActive: false,
        }

        this.handleDrawerStateChange = ::this.handleDrawerStateChange
        this.onChangeLanguageClick = ::this.onChangeLanguageClick
    }

    handleDrawerStateChange () {
        this.setState(prevState => ({
            drawerActive: !prevState.drawerActive,
        }))
    }

    onChangeLanguageClick () {
        const {
            i18n,
        } = this.props
        i18n.changeLanguage(i18n.language == 'ru' ? 'en' : 'ru')
    }

    render () {
        const {
            t,
            i18n,
        } = this.props

        return (
            <header className={this._b}>
                <Link
                    className={this._b('Logo')}
                    to="/"
                >
                    <span
                        className={this._b('Logo')('Icon')}
                    ></span>
                    <span className={this._b('Logo')('Title')}>Statanly</span>
                </Link>

                <div className={this._b('Nav')('DrawerBox')}>
                    <div
                        className={this._b('Nav')('DrawerBox')('Curtain').state({
                            active: this.state.drawerActive,
                        })}
                        onClick={this.handleDrawerStateChange}
                    >
                    </div>
                    <div className={this._b('Nav')('DrawerBox')('Account')}>
                        <UserAccount/>
                    </div>
                    <div
                        className={this._b('Nav')('DrawerBox')('Burger')}
                        onClick={this.handleDrawerStateChange}
                    ></div>
                    <nav
                        className={this._b('Nav').state({
                            active: this.state.drawerActive,
                        })}>
                        <div className={this._b('Nav')('LogoBox')}>
                            <a
                                className={this._b('Nav')('LogoBox')('Logo')}
                            >
                                <span className={this._b('Nav')('LogoBox')('Logo')('Title')}>Statanly</span>
                            </a>
                            <div
                                className={this._b('Nav')('LogoBox')('LanguageSwitcher')}
                                onClick={this.onChangeLanguageClick}>
                                {i18n.language == 'ru' ? 'En' : 'Ru'}
                            </div>
                        </div>
                        <ul className={this._b('Nav')('List')}>
                            <li className={this._b('Nav')('List')('Item')}>
                                <Link className={this._b('Nav')('List')('Item')('Link')} to="/services">{t('Header.Solutions')}</Link>
                            </li>
                            <li className={this._b('Nav')('List')('Item')}>
                                <a
                                    className={this._b('Nav')('List')('Item')('Link')}
                                    href="//api.statanly.com"
                                    target="_blank"
                                >
                                    API
                                </a>
                            </li>
                            <li className={this._b('Nav')('List')('Item')}>
                                <Link className={this._b('Nav')('List')('Item')('Link')} to="/about">{t('Header.About us')}</Link>
                            </li>
                            <li className={this._b('Nav')('List')('Item')}>
                                <Link className={this._b('Nav')('List')('Item')('Link')} to="/help">{t('Header.Contacts')}</Link>
                            </li>
                            <li className={this._b('Nav')('List')('Item')}>
                                <Link className={this._b('Nav')('List')('Item')('Link')} to="/research">{t('Header.Research')}</Link>
                            </li>

                            <li className={this._b('Nav')('List')('Item').mix(['extra', 'border-top'])}>
                                <AuthLoginButton/>
                            </li>
                        </ul>
                        <div className={this._b('Nav')('Right')}>
                            <AuthAccount/>
                            <div
                                className={this._b('Nav')('Right')('LanguageSwitcher')}
                                onClick={() => this.onChangeLanguageClick()}>
                                {i18n.language == 'ru' ? 'En' : 'Ru'}
                            </div>
                        </div>
                    </nav>
                </div>
            </header>
        )
    }
}

export default translate('translations')(Header)
