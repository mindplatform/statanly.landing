import './Technologies.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import Clusterization from 'UI/Components/VisualComponents/Clusterization/Clusterization.react'

import {
    translate,
} from 'react-i18next'

class Technologies extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'Technologies'
        this._b = _b(this.boxClassName)
    }

    render () {
        const {
            t,
        } = this.props

        // <Clusterization className={this._b('Clusterization')}/>

        return (
            <section className={this._b}>
                <h1 className={this._b('Title')}>{t('Technologies.What we do')}</h1>
                <div className={this._b('Box')}>
                    <div className={this._b('Box')('Technology').mix('Prediction')}>
                        <div className={this._b('Box')('Technology')('Icon')}/>
                        <div className={this._b('Box')('Technology')('Title')}>{t('Technologies.Predictive analysis')}</div>
                        <div className={this._b('Box')('Technology')('Description')}>{t('Technologies.Predictive analysis.Description')}</div>
                    </div>
                    <div className={this._b('Box')('Technology').mix('ClusterAnalysis')}>
                        <div className={this._b('Box')('Technology')('Icon')}/>
                        <div className={this._b('Box')('Technology')('Title')}>{t('Technologies.Customer segmentation')}</div>
                        <div className={this._b('Box')('Technology')('Description')}>{t('Technologies.Customer segmentation.Description')}</div>
                    </div>
                    <div className={this._b('Box')('Technology').mix('Visualisation')}>
                        <div className={this._b('Box')('Technology')('Icon')}/>
                        <div className={this._b('Box')('Technology')('Title')}>{t('Technologies.Big data visualisation')}</div>
                        <div className={this._b('Box')('Technology')('Description')}>{t('Technologies.Big data visualisation.Description')}</div>
                    </div>
                    <div className={this._b('Box')('Technology').mix('ChatBots')}>
                        <div className={this._b('Box')('Technology')('Icon')}/>
                        <div className={this._b('Box')('Technology')('Title')}>{t('Technologies.Recommendation systems')}</div>
                        <div className={this._b('Box')('Technology')('Description')}>{t('Technologies.Recommendation systems.Description')}</div>
                    </div>
                    <div className={this._b('Box')('Technology').mix('TimelineDB')}>
                        <div className={this._b('Box')('Technology')('Icon')}/>
                        <div className={this._b('Box')('Technology')('Title')}>{t('Technologies.Statistical database')}</div>
                        <div className={this._b('Box')('Technology')('Description')}>{t('Technologies.Statistical database.Description')}</div>
                    </div>
                    <div className={this._b('Box')('Technology').mix('Classification')}>
                        <div className={this._b('Box')('Technology')('Icon')}/>
                        <div className={this._b('Box')('Technology')('Title')}>{t('Technologies.Intelligent systems')}</div>
                        <div className={this._b('Box')('Technology')('Description')}>{t('Technologies.Intelligent systems.Description')}</div>
                    </div>
                </div>
            </section>
        )
    }
}

export default translate('translations')(Technologies)