import './Register.less'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import _b from 'bem-cn'

import FA from 'react-fontawesome'

import {
    translate,
} from 'react-i18next'

class Register extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'RegisterComponent'
        this._b = _b(this.boxClassName)

        this.handleSubmit = ::this.handleSubmit
    }

    handleSubmit (event) {
        const {
            target,
            nativeEvent,
        } = event

        const {
            isInProcess,
        } = this.props

        if (!isInProcess && target.checkValidity())
            this.props.onSubmit({
                body: new FormData(target),
            })

        nativeEvent.preventDefault()
    }

    renderSubmitButton () {
        const {
            t,
            isInProcess,
        } = this.props

        if (isInProcess)
            return <FA name="spinner" spin/>

        return t('Auth.Ok')
    }

    renderStateButtons () {
        const {
            t,
            isFailure,
        } = this.props

        if (isFailure) {
            return (
                <div className={this._b('Page')('Form')('Error')}>
                    {t('Auth.Error')}
                </div>
            )
        } else {
            return (
                <button className={this._b('Page')('Form')('Button')}>
                    {this.renderSubmitButton()}
                </button>
            )
        }
    }

    render () {
        return (
            <section className={this._b}>
                <div className={this._b('Page')}>
                    <div className={this._b('Page')('Form')}>
                        <form onSubmit={this.handleSubmit}>
                            <input className={this._b('Page')('Form')('Input')} name="Email" type="email" placeholder="mail" required/>
                            <input className={this._b('Page')('Form')('Input')} name="Password" type="password" placeholder="password" required/>
                            <input className={this._b('Page')('Form')('Input')} name="ConfirmPassword" type="password" placeholder="repeat password" required/>
                            {this.renderStateButtons()}
                        </form>
                    </div>
                </div>
            </section>
        )
    }
}

Register.propTypes = {
    onSubmit: PropTypes.func,
    isInProcess: PropTypes.bool,
    isFailure: PropTypes.bool,
}

Register.defaultProps = {
    onSubmit: () => {},
    isInProcess: false,
    isFailure: false,
}

export default translate('translations')(Register)
