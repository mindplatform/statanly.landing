import './Research.less'
import React, { Component } from 'react'
import _b from 'bem-cn'
import {
    translate,
} from 'react-i18next'

class Research extends Component {
    constructor (...args) {
        super(...args)
        this.boxClassName = 'ResearchComponent'
        this._b = _b(this.boxClassName)
    }

    renderLinkItemType (description, link, index) {
        return <a key={index + 'linkType'} target="_blank" href={link} className={this._b('ListBox')('List')('Item')('Body')('DescriptionLinkItem')}>{description}</a>
    }

    renderListItemType (description, index) {
        return <p key={index + 'listType'}  className={this._b('ListBox')('List')('Item')('Body')('DescriptionListItem')}>{description}</p>
    }

    renderDescriptionType (description, index) {
        return <div key={index + 'descriptionType'} className={this._b('ListBox')('List')('Item')('Body')('Description')}>{description}</div>
    }

    renderDescription (description) {
        if (description.map) {
            return (
                <div className={this._b('ListBox')('List')('Item')('Body')('DescriptionList')}>
                    {
                        description.map((v, index) => {
                            if (v.type == 'description') {
                                return this.renderDescriptionType(v.content, index)
                            } else if (v.type == 'listItem') {
                                return this.renderListItemType(v.content, index)
                            }
                        })
                    }
                </div>
            )
        }
        return this.renderDescriptionType(description)
    }

    renderItem ({
        head,
        subhead,
        description,
    }, index) {
        return (
            <li
                key={index + 'service'} 
                className={this._b('ListBox')('List')('Item')}>
                <div className={this._b('ListBox')('List')('Item')('Body')}>
                    {
                        head 
                            ? (
                                <div className={this._b('ListBox')('List')('Item')('Body')('Head')}>
                                    <span className={this._b('ListBox')('List')('Item')('Body')('Head')('Before')}>―</span>
                                    <span className={this._b('ListBox')('List')('Item')('Body')('Head')('Center')}>{head}</span>
                                    <span className={this._b('ListBox')('List')('Item')('Body')('Head')('After')}>―</span>
                                </div>
                            )
                            : null
                    }
                    {
                        subhead
                            ? (
                                <div className={this._b('ListBox')('List')('Item')('Body')('SubHead')}>
                                    {subhead}
                                </div>
                            )
                            : null
                    }
                    {this.renderDescription(description)}
                </div>
            </li>
        )
    }

    render () {
        const {
            t,
        } = this.props

        this.descriptionList = [
            {
                subhead: t('ResearchComponent.The main focus has been put'),
                description: [
                    {
                        type: 'listItem',
                        content: t('ResearchComponent.The main focus has been put.Description[0]'),
                    },
                    {
                        type: 'listItem',
                        content: t('ResearchComponent.The main focus has been put.Description[1]'),
                    },
                    {
                        type: 'listItem',
                        content: t('ResearchComponent.The main focus has been put.Description[2]'),
                    },
                    {
                        type: 'listItem',
                        content: t('ResearchComponent.The main focus has been put.Description[3]'),
                    },{
                        type: 'listItem',
                        content: t('ResearchComponent.The main focus has been put.Description[4]'),
                    },
                    {
                        type: 'listItem',
                        content: t('ResearchComponent.The main focus has been put.Description[5]'),
                    },
                    {
                        type: 'listItem',
                        content: t('ResearchComponent.The main focus has been put.Description[6]'),
                    },
                    {
                        type: 'listItem',
                        content: t('ResearchComponent.The main focus has been put.Description[7]'),
                    },
                    {
                        type: 'listItem',
                        content: t('ResearchComponent.The main focus has been put.Description[8]'),
                    },
                    {
                        type: 'listItem',
                        content: t('ResearchComponent.The main focus has been put.Description[9]'),
                    },
                    {
                        type: 'listItem',
                        content: t('ResearchComponent.The main focus has been put.Description[10]'),
                    },
                ]
            },
        ]

        this.publicationsList = [
            {
                head: t('ResearchComponent.Significant publications'),
                description: [
                    {
                        type: 'listItem',
                        content: t('ResearchComponent.Significant publications.Description[0]'),
                    },
                    {
                        type: 'listItem',
                        content: t('ResearchComponent.Significant publications.Description[1]'),
                    },
                    {
                        type: 'listItem',
                        content: t('ResearchComponent.Significant publications.Description[2]'),
                    },
                    {
                        type: 'listItem',
                        content: t('ResearchComponent.Significant publications.Description[3]'),
                    },{
                        type: 'listItem',
                        content: t('ResearchComponent.Significant publications.Description[4]'),
                    },
                    {
                        type: 'listItem',
                        content: t('ResearchComponent.Significant publications.Description[5]'),
                    },
                ]
            },
        ]

        return (
            <section className={this._b}>
                <div className={this._b('Title')}>
                    {t('ResearchComponent.Title.Main')}
                </div>
                <div className={this._b('Intro')}>
                    {t('ResearchComponent.Description.Main')}
                </div>
                <div className={this._b('ListBox')}>
                    <ul className={this._b('ListBox')('List')}>
                        {this.descriptionList.map((item, index) => this.renderItem(item, index))}
                    </ul>
                </div>
                <div className={this._b('Intro').mix(['Big', 'Center'])}>
                    {t('ResearchComponent.Description.Results of our work are regularly presented in the major scientific journals in data science')}
                </div>
                <div className={this._b('ListBox')}>
                    <ul className={this._b('ListBox')('List')}>
                        {this.publicationsList.map((item, index) => this.renderItem(item, index))}
                    </ul>
                </div>
            </section>
        )
    }
}

export default translate('translations')(Research)