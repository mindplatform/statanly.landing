import './Splash.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import Particles from 'react-particles-js'

import {
    translate,
} from 'react-i18next'

class Splash extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'Splash'
        this._b = _b(this.boxClassName)
    }

    render () {
        const {
            t
        } = this.props

        return (
            <section className={this._b}>
                <div className={this._b('Title')}>
                    <h1 className={this._b('Title')('H1')}>Explore the world of <span className={this._b('Title')('H1')('Data')}>Data</span></h1>
                    <h2 className={this._b('Title')('H2')}>
                        <span className={this._b('Title')('H2')('With').mix('unselectable')}>with </span>
                        <span className={this._b('Title')('H2')('Company')}>Statanly ML-Services</span>
                     </h2>
                </div>
                <Particles
                    className={this._b('Particles')}
                    width="100%"
                    height="100%"
                    style={{
                        position: "absolute",
                        left: 0,
                        top: 0,
                        width: "100%",
                        height: "100%",
                        zIndex: 1,
                    }}
                />
                <div className={this._b('Mask')}/>
            </section>
        )
    }
}

export default translate('translations')(Splash)