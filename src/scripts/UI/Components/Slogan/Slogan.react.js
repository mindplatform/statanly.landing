import './Slogan.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import {
    translate,
} from 'react-i18next'

class Slogan extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'Slogan'
        this._b = _b(this.boxClassName)
    }

    render () {
        const {
            t,
        } = this.props

        return (
            <section className={this._b}>
                <h1 className={this._b('Title')}>
                    {t('Slogan.Machine learning services for data analysis and visualisation')}
                </h1>
            </section>
        )
    }
}

export default translate('translations')(Slogan)
