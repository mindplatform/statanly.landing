import './Partners.less'
import React, { Component } from 'react'
import _b from 'bem-cn'
import { Link } from 'react-router-dom'

import { 
    translate,
} from 'react-i18next'

class Partners extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'Partners'
        this._b = _b(this.boxClassName)
    }

    render () {
        const {
            t,
        } = this.props

        return (
            <section className={this._b}>
                <ul className={this._b('List')}>
                    <li className={this._b('List')('Item')}>
                        <span className={this._b('List')('Item')('Inner')}>{t('Partners.Extract valuable insights from your data')}</span>
                    </li>
                    <li className={this._b('List')('Item')}>
                        <span className={this._b('List')('Item')('Inner')}>{t('Partners.Receive high-precision forecasts')}</span>
                    </li>
                    <li className={this._b('List')('Item')}>
                        <span className={this._b('List')('Item')('Inner')}>{t('Partners.Maximize effectiveness of data analysis')}</span>
                    </li>
                    <li className={this._b('List')('Item')}>
                        <span className={this._b('List')('Item')('Inner')}>{t('Partners.Visualise results of data analysis and identify hidden correlations')}</span>
                    </li>
                    <li className={this._b('List')('Item')}>
                        <span className={this._b('List')('Item')('Inner')}>{t('Partners.Deploy our solutions in your projects and applications')}</span>
                    </li>
                    <li className={this._b('List')('Item')}>
                        <span className={this._b('List')('Item')('Inner')}>{t('Partners.Discover full potential of machine learning algorithms')}</span>
                    </li>
                </ul>
                <div className={this._b('ButtonBox')}>
                    <Link  
                        className={this._b('ButtonBox')('Button')}
                        to="/services"
                    >
                        {t('Partners.More')}
                    </Link>
                </div>
            </section>
        )
    }

    // render () {
    //     return (
    //         <section className={this._b}>
    //             <ul className={this._b('List')}>
    //                 <li className={this._b('List')('Item')}>
    //                     <span className={this._b('List')('Item')('Inner')}>Извлекайте полезную информацию из ваших данных</span>
    //                 </li>
    //                 <li className={this._b('List')('Item')}>
    //                     <span className={this._b('List')('Item')('Inner')}>Получайте высокоточные прогнозы и рекомендации</span>
    //                 </li>
    //                 <li className={this._b('List')('Item')}>
    //                     <span className={this._b('List')('Item')('Inner')}>Используйте ваши данные с максимальной эффективностью</span>
    //                 </li>
    //                 <li className={this._b('List')('Item')}>
    //                     <span className={this._b('List')('Item')('Inner')}>Визуализируйте результаты анализа и выявляйте скрытые закономерности</span>
    //                 </li>
    //                 <li className={this._b('List')('Item')}>
    //                     <span className={this._b('List')('Item')('Inner')}>Внедряйте наши решения в свои проекты и приложения</span>
    //                 </li>
    //                 <li className={this._b('List')('Item')}>
    //                     <span className={this._b('List')('Item')('Inner')}>Оцените всю мощь интеллектуальных алгоритмов машинного обучения</span>
    //                 </li>
    //             </ul>
    //             <div className={this._b('ButtonBox')}>
    //                 <a 
    //                     className={this._b('ButtonBox')('Button')}
    //                     href="/services"
    //                 >
    //                     Подробнее
    //                 </a>
    //             </div>
    //         </section>
    //     )
    // }
}

export default translate('translations')(Partners)