import './Login.less'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import _b from 'bem-cn'

import FA from 'react-fontawesome'

import {
    translate,
} from 'react-i18next'

class Login extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'LoginComponent'
        this._b = _b(this.boxClassName)

        this.handleSubmit = ::this.handleSubmit
    }

    handleSubmit (event) {
        const {
            target,
            nativeEvent,
        } = event

        const {
            isInProcess,
        } = this.props

        if (!isInProcess && target.checkValidity())
            this.props.onSubmit({
                body: new FormData(target),
            })

        nativeEvent.preventDefault()
    }

    renderSubmitButton () {
        const {
            t,
            isInProcess,
        } = this.props

        if (isInProcess)
            return <FA name="spinner" spin/>

        return t('Auth.Sign In')
    }

    renderStateButtons () {
        const {
            t,
            isFailure,
        } = this.props

        if (isFailure) {
            return (
                <div className={this._b('Page')('Form')('Error')}>
                    {t('Auth.Error')}
                </div>
            )
        } else {
            return (
                <button className={this._b('Page')('Form')('Button')}>
                    {this.renderSubmitButton()}
                </button>
            )
        }
    }

    render () {
        const {
            t,
        } = this.props

        return (
            <section className={this._b}>
                <div className={this._b('Page')}>
                    <div className={this._b('Page')('Form')}>
                        <form onSubmit={this.handleSubmit}>
                            <input className={this._b('Page')('Form')('Input')} name="Email" type="email" placeholder="mail" required/>
                            <input className={this._b('Page')('Form')('Input')} name="Password" type="password" placeholder="password" required/>
                            <label className={this._b('Page')('Form')('Label').mix('unselectable')} htmlFor="loginformrememberme">
                                <span className={this._b('Page')('Form')('Label')('Text')}>{t('Auth.Remember me?')}</span>
                                <input className={this._b('Page')('Form')('Label')('Input')} id="loginformrememberme" name="RememberMe" type="checkbox" value="true"/>
                            </label>
                            {this.renderStateButtons()}
                        </form>
                        <a className={this._b('Page')('Form')('Register')} href="/Account/Register">
                            {t('Auth.Sign Up')}
                        </a>
                    </div>
                </div>
            </section>
        )
    }
}

Login.propTypes = {
    onSubmit: PropTypes.func,
    isInProcess: PropTypes.bool,
    isFailure: PropTypes.bool,
}

Login.defaultProps = {
    onSubmit: () => {},
    isInProcess: false,
    isFailure: false,
}

export default translate('translations')(Login)
