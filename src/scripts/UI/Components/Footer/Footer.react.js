import './Footer.less'
import React, { Component } from 'react'
import _b from 'bem-cn'
import { Link } from 'react-router-dom'
import FA from 'react-fontawesome'
import { 
    translate,
} from 'react-i18next'

class Footer extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'Footer'
        this._b = _b(this.boxClassName)
    }

    render () {
        const {
            t,
        } = this.props

        return (
            <section className={this._b}>
                <div className={this._b('Blocks')}>
                    <div className={this._b('Blocks')('Block')}>
                        <p className={this._b('Blocks')('Block')('Title')}>{t('Footer.Company')}</p>
                        <ul className={this._b('Blocks')('Block')('List')}>
                            <li className={this._b('Blocks')('Block')('List')('Item')}>
                                <Link className={this._b('Blocks')('Block')('Link')} to="/about">{t('Footer.About us')}</Link>
                            </li>
                            <li className={this._b('Blocks')('Block')('List')('Item')}>
                                <Link className={this._b('Blocks')('Block')('Link')} to="/help">{t('Footer.Help')}</Link>
                            </li>
                            <li className={this._b('Blocks')('Block')('List')('Item')}>
                                <Link className={this._b('Blocks')('Block')('Link')} to="/help">{t('Footer.Contacts')}</Link>
                            </li>
                        </ul>
                    </div>
                    <div className={this._b('Blocks')('Block')}>
                        <p className={this._b('Blocks')('Block')('Title')}>{t('Footer.What we do')}</p>
                        <ul className={this._b('Blocks')('Block')('List')}>
                            <li className={this._b('Blocks')('Block')('List')('Item')}>
                                <Link className={this._b('Blocks')('Block')('Link')} to="/services">{t('Footer.Intelligent systems')}</Link>
                            </li>
                            <li className={this._b('Blocks')('Block')('List')('Item')}>
                                <Link className={this._b('Blocks')('Block')('Link')} to="/services">{t('Footer.Machine learning')}</Link>
                            </li>
                            <li className={this._b('Blocks')('Block')('List')('Item')}>
                                <Link className={this._b('Blocks')('Block')('Link')} to="/services">{t('Footer.Big data')}</Link>
                            </li>
                            <li className={this._b('Blocks')('Block')('List')('Item')}>
                                <Link className={this._b('Blocks')('Block')('Link')} to="/services">{t('Footer.Neural networks')}</Link>
                            </li>
                        </ul>
                    </div>
                    <div className={this._b('Blocks')('Block')}>
                        <p className={this._b('Blocks')('Block')('Title')}>{t('Footer.Open code')}</p>
                        <ul className={this._b('Blocks')('Block')('List')}>
                            <li className={this._b('Blocks')('Block')('List')('Item')}>
                                <Link className={this._b('Blocks')('Block')('Link')} to="/#">{t('Footer.User agreement')}</Link>
                            </li>
                            <li className={this._b('Blocks')('Block')('List')('Item')}>
                                <Link className={this._b('Blocks')('Block')('Link')} to="/#">{t('Footer.Source code')}</Link>
                            </li>
                            <li className={this._b('Blocks')('Block')('List')('Item')}>
                                <Link className={this._b('Blocks')('Block')('Link')} to="/#">{t('Footer.Our algorithms')}</Link>
                            </li>
                            <li className={this._b('Blocks')('Block')('List')('Item')}>
                                <Link className={this._b('Blocks')('Block')('Link')} to="/#">{t('Footer.Resources')}</Link>
                            </li>
                        </ul>
                    </div>
                    <div className={this._b('Blocks')('Block')}>
                        <p className={this._b('Blocks')('Block')('Title')}>{t('Footer.Contacts')}</p>
                        <ul className={this._b('Blocks')('Block')('List')}>
                            <li className={this._b('Blocks')('Block')('List')('Item')}><a className={this._b('Blocks')('Block')('Link')} href="tel:+7-921-875-23-96">{t('Footer.Phone')}: +7 921 875 23 96</a></li>
                            <li className={this._b('Blocks')('Block')('List')('Item')}><a className={this._b('Blocks')('Block')('Link')} href="mailto:hello@statanly.com">{t('Footer.Email')}: hello@statanly.com</a></li>
                            <li className={this._b('Blocks')('Block')('List')('Item')}>
                                <a className={this._b('Blocks')('Block')('List')('Item')('Link')} href="https://www.facebook.com/statanly" target="_blank">
                                    <FA name="facebook-f" className={this._b('Blocks')('Block')('List')('Item')('Icon').toString()}/>
                                </a>
                                <a className={this._b('Blocks')('Block')('List')('Item')('Link')} href="https://twitter.com/statanly" target="_blank">
                                    <FA name="twitter" className={this._b('Blocks')('Block')('List')('Item')('Icon').toString()}/>
                                </a>
                                <a className={this._b('Blocks')('Block')('List')('Item')('Link')} href="https://www.instagram.com/statanly" target="_blank">
                                    <FA name="instagram" className={this._b('Blocks')('Block')('List')('Item')('Icon').toString()}/>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <p className={this._b('License')}>{t('Footer.License')}</p>
            </section>
        )
    }
}

export default translate('translations')(Footer)