import './HowDoesItWorkTab.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import { 
    translate,
} from 'react-i18next'

class HowDoesItWorkTab extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'HowDoesItWorkTab'
        this._b = _b(this.boxClassName)
    }

    renderImage (image) {
        if (!image)
            return null

        return <img src={image} className={this._b('ListBox')('List')('Item')('Body')('Image')}/>
    }

    renderListItemType (description, index) {
        return <p key={index + 'listType'}  className={this._b('ListBox')('List')('Item')('Body')('DescriptionListItem')}>{description}</p>
    }

    renderDescriptionType (description, index) {
        return <div key={index + 'descriptionType'} className={this._b('ListBox')('List')('Item')('Body')('Description')}>{description}</div>
    }

    renderDescription (description) {
        if (description && description.map) {
            return (
                <div className={this._b('ListBox')('List')('Item')('Body')('DescriptionList')}>
                    {
                        description.map((v, index) => {
                            if (v.type == 'description') {
                                return this.renderDescriptionType(v.content, index)
                            } else if (v.type == 'listItem') {
                                return this.renderListItemType(v.content, index)
                            }
                        })
                    }
                </div>
            )
        }
        return this.renderDescriptionType(description)
    }

    renderItem ({
        head,
        image,
        description,
    }, index) {
        return (
            <li
                key={index + 'service'} 
                className={this._b('ListBox')('List')('Item')}>
                <div className={this._b('ListBox')('List')('Item')('Body')}>
                    <div className={this._b('ListBox')('List')('Item')('Body')('Head')}>
                        <span className={this._b('ListBox')('List')('Item')('Body')('Head')('Center')}>{head}</span>
                    </div>
                    {this.renderImage(image)}
                    {this.renderDescription(description)}
                </div>
            </li>
        )
    }

    render () {

        const {
            t,
        } = this.props

        this.howDoesItWorkList = [
            {
                head: t('HowDoesItWorkTab.Upload your data in any convenient way or automatically via our API'),
                image: '/content/spa/images/services/howto/upload.png',
            },
            {
                head: t('HowDoesItWorkTab.Select among the tasks forecasting, classification or visualisation'),
                description: [
                    {
                        type: 'listItem',
                        content: t('HowDoesItWorkTab.Select among the tasks forecasting, classification or visualisation.List[0]'),
                    },
                    {
                        type: 'listItem',
                        content: t('HowDoesItWorkTab.Select among the tasks forecasting, classification or visualisation.List[1]'),
                    },
                    {
                        type: 'listItem',
                        content: t('HowDoesItWorkTab.Select among the tasks forecasting, classification or visualisation.List[2]'),
                    },
                ],
                image: '',
            },
            {
                head: t('HowDoesItWorkTab.Statanly brain quickly solves the task. Get results in forecasts, recommendations or visual maps'),
                image: '/content/spa/images/services/howto/explore.png',
            },
        ]

        return (
            <section className={this._b}>
                <div className={this._b('Title')}>
                    {t('HowDoesItWorkTab.Title.Main')}
                </div>
                <div className={this._b('ListBox')}>
                    <ul className={this._b('ListBox')('List')}>
                        
                        {this.howDoesItWorkList.map((item, index) => this.renderItem(item, index))}

                    </ul>
                </div>
            </section>
        )
    }
}

export default translate('translations')(HowDoesItWorkTab)