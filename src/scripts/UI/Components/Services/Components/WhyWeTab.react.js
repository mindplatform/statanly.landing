import './WhyWeTab.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import { 
    translate,
} from 'react-i18next'

class WhyWeTab extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'WhyWeTab'
        this._b = _b(this.boxClassName)
    }

    renderItem ({
        head,
        description,
    }, index) {
        return (
            <li
                key={index + 'service'} 
                className={this._b('ListBox')('List')('Item')}>
                <div className={this._b('ListBox')('List')('Item')('Body')}>
                    <div className={this._b('ListBox')('List')('Item')('Body')('Head')}>
                        <span className={this._b('ListBox')('List')('Item')('Body')('Head')('Before')}></span>
                        <span className={this._b('ListBox')('List')('Item')('Body')('Head')('Center')}>{head}</span>
                        <span className={this._b('ListBox')('List')('Item')('Body')('Head')('After')}></span>
                    </div>
                    <div className={this._b('ListBox')('List')('Item')('Body')('Description')}>{description}</div>
                </div>
            </li>
        )
    }

    render () {

        const {
            t,
        } = this.props

        this.descriptionList = [
            {
                head: t('WhyWeTab.Easy to use and intuitive interface'),
            },
            {
                head: t('WhyWeTab.Supported integration with main file storages yandex disk, google drive, dropbox'),
            },
            {
                head: t('WhyWeTab.High task processing speed and interpretability of the results'),
            },
            {
                head: t('WhyWeTab.Easy integration via API and embedded libraries'),
            },
            {
                head: t('WhyWeTab.Open source code'),
            },
            {
                head: t('WhyWeTab.Full-functional free version and affordable advanced subscription options'),
            },
        ]

        return (
            <section className={this._b}>
                <div className={this._b('Title')}>
                    {t('WhyWeTab.Title.Main')}
                </div>
                <div className={this._b('ListBox')}>
                    <ol className={this._b('ListBox')('List')}>
                        
                        {this.descriptionList.map((item, index) => this.renderItem(item, index))}

                    </ol>
                </div>
            </section>
        )
    }
}

export default translate('translations')(WhyWeTab)