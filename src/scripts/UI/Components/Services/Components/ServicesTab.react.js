import './ServicesTab.less'
import React, { Component } from 'react'
import _b from 'bem-cn'

import { 
    translate,
} from 'react-i18next'

class ServicesTab extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'ServicesTab'
        this._b = _b(this.boxClassName)
    }

    renderItem ({
        head,
        description,
    }, index) {
        return (
            <li
                key={index + 'service'} 
                className={this._b('ListBox')('List')('Item')}>
                <div className={this._b('ListBox')('List')('Item')('Body')}>
                    <div className={this._b('ListBox')('List')('Item')('Body')('Head')}>
                        <span className={this._b('ListBox')('List')('Item')('Body')('Head')('Before')}>―</span>
                        <span className={this._b('ListBox')('List')('Item')('Body')('Head')('Center')}>{head}</span>
                        <span className={this._b('ListBox')('List')('Item')('Body')('Head')('After')}>―</span>
                    </div>
                    <div className={this._b('ListBox')('List')('Item')('Body')('Description')}>{description}</div>
                </div>
            </li>
        )
    }

    render () {

        const {
            t,
        } = this.props

        this.descriptionList = [
            {
                head: t('ServicesTab.Predictive analysis'),
                description: t('ServicesTab.Predictive analysis.Description'),
            },
            {
                head: t('ServicesTab.Recommendation systems'),
                description: t('ServicesTab.Recommendation systems.Description'),
            },
            {
                head: t('ServicesTab.Customer segmentation'),
                description: t('ServicesTab.Customer segmentation.Description'),
            },
            {
                head: t('ServicesTab.Statistical database'),
                description: t('ServicesTab.Statistical database.Description'),
            },
        ]

        return (
            <section className={this._b}>
                <div className={this._b('Title')}>
                    {t('ServicesTab.Title.Main')}
                </div>
                <div className={this._b('Intro')}>
                    {t('ServicesTab.Description.Main')}
                </div>
                <div className={this._b('ListBox')}>
                    <ul className={this._b('ListBox')('List')}>
                        
                        {this.descriptionList.map((item, index) => this.renderItem(item, index))}

                    </ul>
                </div>
            </section>
        )
    }
}

export default translate('translations')(ServicesTab)