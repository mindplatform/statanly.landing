import './Services.less'
import React, { Component } from 'react'
import _b from 'bem-cn'
import { 
    Link,
    Route,
} from 'react-router-dom'
import {
    withRouter,
    Switch,
    Redirect,
} from 'react-router'
import { 
    translate,
} from 'react-i18next'

import ServicesTab from './Components/ServicesTab.react'
import HowDoesItWorkTab from './Components/HowDoesItWorkTab.react'
import WhyWeTab from './Components/WhyWeTab.react'

class Services extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'ServicesComponent'
        this._b = _b(this.boxClassName)
    }

    componentDidMount () {
        if (~this.props.location.pathname.indexOf('howto') || ~this.props.location.pathname.indexOf('whywe'))
            return

        this.props.history.replace(`${this.props.match.path}/core`)
    }

    renderNavItem (item, index) {
        return (
            <li
                key={index + 'about-nav-item'}
                className={this._b('Nav')('Item')}
            >
                <Link to={item.link} className={this._b('Nav')('Item')('Link')}>
                    {item.title}
                </Link>
            </li>
        )
    }

    renderNav () {
        const {
            t,
            match,
        } = this.props

        this.navList = [
            {
                link: `${match.path}/core`,
                title: t('Services.Nav.Services'),
            },
            {
                link: `${match.path}/howto`,
                title: t('Services.Nav.HowTo'),
            },
            {
                link: `${match.path}/whywe`,
                title: t('Services.Nav.WhyWe'),
            },
        ]

        return (
            <ul className={this._b('Nav')}>
                {this.navList.map((...args) => this.renderNavItem(...args))}
            </ul>
        )
    }

    render () {
        return (
            <section className={this._b}>
                {this.renderNav()}
                <Route path={`${this.props.match.path}/core`} component={ServicesTab}/>
                <Route path={`${this.props.match.path}/howto`} component={HowDoesItWorkTab}/>
                <Route path={`${this.props.match.path}/whywe`} component={WhyWeTab}/>
            </section>
        )
    }
}

export default withRouter(translate('translations')(Services))