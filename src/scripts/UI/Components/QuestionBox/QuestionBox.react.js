import './QuestionBox.less'
import React, { Component } from 'react'
import bem from 'Helpers/Bem/Bem.helper'

import {
    Link,
} from 'react-router-dom'

import {
    translate,
} from 'react-i18next'

class QuestionBox extends Component {
    constructor (props) {
        super(props)
        this.boxClassName = 'QuestionBox'
        this.bem = bem.with(this.boxClassName)
    }

    render () {
        const {
            t,
        } = this.props

        return (
            <div className={this.bem()}>
                <div className={this.bem('Message')}>
                    {t('QuestionBox.Title')}
                </div>
                <Link className={this.bem('Button')} to={"/help"}>
                    {t('QuestionBox.Button')}
                </Link>
            </div>
        )
    }
}

export default translate('translations')(QuestionBox)
