import './App.less'
import React, { Component } from 'react'
import {
    Route,
 } from 'react-router-dom'
import bem from 'Helpers/Bem/Bem.helper'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Home from 'UI/Pages/Home/Home.react'
import Services from 'UI/Pages/Services/Services.react'
import About from 'UI/Pages/About/About.react'
import Help from 'UI/Pages/Help/Help.react'
import Research from 'UI/Pages/Research/Research.react'

import Login from 'UI/Pages/Login/Login.react'
import Register from 'UI/Pages/Register/Register.react'

import * as UserAccountActions from 'Redux/Actions/UserAccount/UserAccount.actions'

class App extends Component {
    constructor (props) {
        super(props)
        this.blockName = 'App'
        this.bem = bem.with(this.blockName)

        if (window.userInfo) {
            const {
                isAuthenticated,
                email,
            } = window.userInfo

            if (isAuthenticated)
                props.UserAccountActions.addUserAccountInfo({
                    isAuthenticated,
                    email,
                })
        }
    }

    render () {
        return (
            <section
                className={this.bem()}>
                <Route exact path="/" component={Home}/>
                <Route path="/services" component={Services}/>
                <Route path="/about" component={About}/>
                <Route exact path="/help" component={Help}/>
                <Route exact path="/research" component={Research}/>
                <Route exact path="/Account/Register" component={Register}/>
                <Route exact path="/Account/Login" component={Login}/>
            </section>
        )
    }
}

export default connect(
    state => ({}),
    dispatch => ({
        UserAccountActions: bindActionCreators(UserAccountActions, dispatch),
    }),
)(App)
