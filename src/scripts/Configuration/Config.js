import I from 'immutable'

const ENV = process.env.NODE_ENV
const GIT_HASH = process.env.GIT_HASH

const CONFIG_DEV = I.Map({
    API_ROOT_HTTP: null,
    GIT_HASH,
})

const CONFIG_PROD = I.Map({
    API_ROOT_HTTP: null,
    GIT_HASH,
})

const CONFIG = ENV == 'development' ? CONFIG_DEV : CONFIG_PROD

export default CONFIG