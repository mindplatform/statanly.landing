import I from 'immutable'
export default I.fromJS({
    auth: {
        login: {
            path: 'Account/LoginAsync',
            params: {
                method: 'POST',
                headers: {},
                credentials: 'include',
            },
            search: {},
        },
        register: {
            path: 'Account/RegisterAsync',
            params: {
                method: 'POST',
                headers: {},
                credentials: 'include',
            },
            search: {},
        },
    },
    interactive: {
        feedback: {
            path: 'Message/SendAsync',
            params: {
                method: 'POST',
                headers: {},
                credentials: 'include',
            },
            search: {},
        },
        request: {
            path: 'Message/SendRequestAsync',
            params: {
                method: 'POST',
                headers: {},
                credentials: 'include',
            },
            search: {},
        }
    },
})
